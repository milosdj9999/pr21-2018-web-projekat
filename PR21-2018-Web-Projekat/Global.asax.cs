﻿using PR21_2018_Web_Projekat.Models;
using PR21_2018_Web_Projekat.Models.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Xml;

namespace PR21_2018_Web_Projekat
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            #region Aranzmani 

            List<Aranzman> aranzmani = new List<Aranzman>();

            string filepath = HostingEnvironment.MapPath("~/App_Data/Aranzmani.xml");

            XmlDocument doc = new XmlDocument();
            doc.Load(filepath);

            CultureInfo cultures = new CultureInfo("en-US");

            foreach (XmlNode n in doc.DocumentElement.SelectNodes("/Aranzmani/Aranzman"))
            {

                string naziv = n.SelectSingleNode("naziv").InnerText;
                string tipAranzmana = n.SelectSingleNode("tipAranzmana").InnerText;
                string tipPrevoza = n.SelectSingleNode("tipPrevoza").InnerText;
                string lokacija = n.SelectSingleNode("lokacija").InnerText;
                DateTime datumPocetkaPutovanja = Convert.ToDateTime(n.SelectSingleNode("datumPocetkaPutovanja").InnerText, cultures);
                DateTime datumZavrsetkaPutovanja = Convert.ToDateTime(n.SelectSingleNode("datumZavrsetkaPutovanja").InnerText, cultures);


                string adresa = n.SelectSingleNode("mestoNalazenja/adresa").InnerText;
                double geoDuzina = Double.Parse(n.SelectSingleNode("mestoNalazenja/geoDuzina").InnerText);
                double geoSirina = Double.Parse(n.SelectSingleNode("mestoNalazenja/geoSirina").InnerText);
                MestoNalazenja mesto = new MestoNalazenja(adresa, geoDuzina, geoSirina);

                TimeSpan vremeNalazenja = TimeSpan.Parse(n.SelectSingleNode("vremeNalazenja").InnerText);


                int maxBrojPutnika = Int32.Parse(n.SelectSingleNode("maxBrojPutnika").InnerText);
                string opisAranzmana = n.SelectSingleNode("opisAranzmana").InnerText;
                string programPutovanja = n.SelectSingleNode("programPutovanja").InnerText;
                string posterAranzmana = n.SelectSingleNode("posterAranzmana").InnerText;
                bool obrisan = (n.SelectSingleNode("obrisan").InnerText == "Da") ? true : false;

                string tipSmestaja = n.SelectSingleNode("smestaj/tipSmestaja").InnerText;
                string nazivSmestaja = n.SelectSingleNode("smestaj/naziv").InnerText;
                int brojZvezdica = Int32.Parse(n.SelectSingleNode("smestaj/brojZvezdica").InnerText);
                bool postojanjeBazena = (n.SelectSingleNode("smestaj/postojanjeBazena").InnerText == "Da") ? true : false;
                bool postojanjeSpaCentra = (n.SelectSingleNode("smestaj/postojanjeSpaCentra").InnerText == "Da") ? true : false;
                bool invaliditet = (n.SelectSingleNode("smestaj/invaliditet").InnerText == "Da") ? true : false;
                bool wifi = (n.SelectSingleNode("smestaj/wifi").InnerText == "Da") ? true : false;
                bool smestajObrisan = (n.SelectSingleNode("smestaj/obrisan").InnerText == "Da") ? true : false;
                

                List<SmestajnaJedinica> jedinice = new List<SmestajnaJedinica>();

                XmlNode s = n.SelectSingleNode("smestaj/smestajneJedinice");


                foreach (XmlNode n1 in s.ChildNodes)
                {
                    if (n1.Name == "jedinica")
                    {
                        int broj = Int32.Parse(n1.SelectSingleNode("brojSmestajneJedinice").InnerText);
                        int dozvoljenBrGostiju = Int32.Parse(n1.SelectSingleNode("dozvoljenBrGostiju").InnerText);
                        bool ljubimci = (n1.SelectSingleNode("ljubimci").InnerText == "Da") ? true : false;
                        int cena = Int32.Parse(n1.SelectSingleNode("cena").InnerText);
                        bool zauzeta = (n1.SelectSingleNode("zauzeta").InnerText == "Da") ? true : false;
                        bool obrisana = (n1.SelectSingleNode("obrisan").InnerText == "Da") ? true : false;
                        SmestajnaJedinica sj = new SmestajnaJedinica(broj, dozvoljenBrGostiju, ljubimci, cena, zauzeta, obrisana);

                        jedinice.Add(sj);
                    }
                }




                Smestaj smestaj = new Smestaj(tipSmestaja, nazivSmestaja, brojZvezdica, postojanjeBazena, postojanjeSpaCentra, invaliditet, wifi, jedinice, smestajObrisan);

                Aranzman a = new Aranzman(naziv, tipAranzmana, tipPrevoza, lokacija, datumPocetkaPutovanja, datumZavrsetkaPutovanja, mesto, vremeNalazenja,
                    maxBrojPutnika, opisAranzmana, programPutovanja, posterAranzmana, smestaj, obrisan);

                aranzmani.Add(a);

            }

            HttpContext.Current.Application["ARANZMANI"] = aranzmani;

            #endregion

            #region Turisti

            List<Turista> turisti = new List<Turista>();

            filepath = HostingEnvironment.MapPath("~/App_Data/Turisti.xml");


            doc.Load(filepath);

            foreach (XmlNode n in doc.DocumentElement.SelectNodes("/Turisti/Turista"))
            {



                string korisnickoIme = n.SelectSingleNode("korisnickoIme").InnerText;
                string lozinka = n.SelectSingleNode("lozinka").InnerText;
                string ime = n.SelectSingleNode("ime").InnerText;
                string prezime = n.SelectSingleNode("prezime").InnerText;
                string pol = n.SelectSingleNode("pol").InnerText;
                string email = n.SelectSingleNode("email").InnerText;
                bool obrisan = (n.SelectSingleNode("obrisan").InnerText == "Da") ? true : false;
                DateTime datumRodjenja = Convert.ToDateTime(n.SelectSingleNode("datumRodjenja").InnerText, cultures);

                List<Rezervacija> rezervacije = new List<Rezervacija>();

                XmlNode s = n.SelectSingleNode("rezervacije");


                foreach (XmlNode n1 in s.ChildNodes)
                {

                    string jir = n1.SelectSingleNode("jir").InnerText;
                    string imeTuriste = n1.SelectSingleNode("imeTuriste").InnerText;
                    bool status = (n1.SelectSingleNode("status").InnerText == "Aktivna") ? true : false;
                    string aranzman = n1.SelectSingleNode("aranzman").InnerText; ;
                    int brojSJ = Int32.Parse(n1.SelectSingleNode("smestajnaJedinica").InnerText);

                    SmestajnaJedinica smestajnaJedinica = null;

                    foreach (Aranzman a in aranzmani)
                    {
                        if (a.Naziv == aranzman)
                        {
                            foreach (SmestajnaJedinica sj in a.Smestaj.SmestajneJedinice)
                            {
                                if (sj.BrojSmestajneJedinice == brojSJ)
                                {
                                    smestajnaJedinica = sj;
                                    break;
                                }
                            }
                        }
                    }
                    Rezervacija rez = new Rezervacija(jir, imeTuriste, status, aranzman, smestajnaJedinica);

                    rezervacije.Add(rez);
                }


                Turista t = new Turista(korisnickoIme, lozinka, ime, prezime, pol, email, datumRodjenja, EUloga.turista, rezervacije, obrisan);

                turisti.Add(t);
            }

            HttpContext.Current.Application["TURISTI"] = turisti;


            #endregion

            #region Komentari
            List<Komentar> komentari = new List<Komentar>();

            filepath = HostingEnvironment.MapPath("~/App_Data/Komentari.xml");

            doc = new XmlDocument();
            doc.Load(filepath);


            foreach (XmlNode n in doc.DocumentElement.SelectNodes("/Komentari/Komentar"))
            {
                string turista = n.SelectSingleNode("turista").InnerText;
                string aranzman = n.SelectSingleNode("aranzman").InnerText;
                string tekst = n.SelectSingleNode("tekst").InnerText;
                int ocena = Int32.Parse(n.SelectSingleNode("ocena").InnerText);
                bool vidljiv = (n.SelectSingleNode("vidljiv").InnerText == "Da" ? true : false);

                Komentar k = new Komentar(turista, aranzman, tekst, ocena, vidljiv);
                komentari.Add(k);
            }

                HttpContext.Current.Application["KOMENTARI"] = komentari;

            #endregion

            #region Menadzeri

            List<Menadzer> menadzeri = new List<Menadzer>();

            filepath = HostingEnvironment.MapPath("~/App_Data/Menadzeri.xml");


            doc.Load(filepath);

            foreach (XmlNode n in doc.DocumentElement.SelectNodes("/Menadzeri/Menadzer"))
            {



                string korisnickoIme = n.SelectSingleNode("korisnickoIme").InnerText;
                string lozinka = n.SelectSingleNode("lozinka").InnerText;
                string ime = n.SelectSingleNode("ime").InnerText;
                string prezime = n.SelectSingleNode("prezime").InnerText;
                string pol = n.SelectSingleNode("pol").InnerText;
                string email = n.SelectSingleNode("email").InnerText;
                DateTime datumRodjenja = Convert.ToDateTime(n.SelectSingleNode("datumRodjenja").InnerText, cultures);

                List<Aranzman> aranzmaniMenadzer = new List<Aranzman>();

                Aranzman temp = null; 

                XmlNode s = n.SelectSingleNode("aranzmani");


                foreach (XmlNode n1 in s.ChildNodes)
                {
                    foreach (Aranzman a in aranzmani)
                    {
                        if (a.Naziv == n1.InnerText)
                        {
                            temp = a;
                        }
                    }

                    aranzmaniMenadzer.Add(temp);
                   
                }

                Menadzer m = new Menadzer(korisnickoIme, lozinka, ime, prezime, pol, email, datumRodjenja, EUloga.menadzer, aranzmaniMenadzer);

                menadzeri.Add(m);
            }

            HttpContext.Current.Application["MENADZERI"] = menadzeri;

            #endregion

            #region Administratori

            List<Administrator> administratori = new List<Administrator>();

            filepath = HostingEnvironment.MapPath("~/App_Data/Administratori.xml");


            doc.Load(filepath);

            foreach (XmlNode n in doc.DocumentElement.SelectNodes("/Administratori/Administrator"))
            {



                string korisnickoIme = n.SelectSingleNode("korisnickoIme").InnerText;
                string lozinka = n.SelectSingleNode("lozinka").InnerText;
                string ime = n.SelectSingleNode("ime").InnerText;
                string prezime = n.SelectSingleNode("prezime").InnerText;
                string pol = n.SelectSingleNode("pol").InnerText;
                string email = n.SelectSingleNode("email").InnerText;
                DateTime datumRodjenja = Convert.ToDateTime(n.SelectSingleNode("datumRodjenja").InnerText, cultures);



                Administrator a = new Administrator(korisnickoIme, lozinka, ime, prezime, pol, email, datumRodjenja, EUloga.administrator);

                administratori.Add(a);
            }

            HttpContext.Current.Application["ADMINISTRATORI"] = administratori;


            #endregion

        }



    }
}
