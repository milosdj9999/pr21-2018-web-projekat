﻿using PR21_2018_Web_Projekat.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR21_2018_Web_Projekat.Models
{
    public class Aranzman
    {
        public string naziv;
        string tipAranzmana;
        string tipPrevoza;   
        string lokacija;
        DateTime datumPocetkaPutovanja;
        DateTime datumZavrsetkaPutovanja;
        MestoNalazenja mestoNalazenja;
        TimeSpan vremeNalazenja;  
        int maxBrojPutnika;
        string opisAranzmana;
        string programPutovanja;
        string posterAranzmana;
        Smestaj smestaj;
        bool obrisan;

        public Aranzman(string naziv, string tipAranzmana, string tipPrevoza, string lokacija, DateTime datumPocetkaPutovanja, DateTime datumZavrsetkaPutovanja, MestoNalazenja mestoNalazenja, TimeSpan vremeNalazenja, int maxBrojPutnika, string opisAranzmana, string programPutovanja, string posterAranzmana, Smestaj smestaj, bool obrisan)
        {
            Naziv = naziv;
            TipAranzmana = tipAranzmana;
            TipPrevoza = tipPrevoza;
            Lokacija = lokacija;
            DatumPocetkaPutovanja = datumPocetkaPutovanja;
            DatumZavrsetkaPutovanja = datumZavrsetkaPutovanja;
            MestoNalazenja = mestoNalazenja;
            VremeNalazenja = vremeNalazenja;
            MaxBrojPutnika = maxBrojPutnika;
            OpisAranzmana = opisAranzmana;
            ProgramPutovanja = programPutovanja;
            PosterAranzmana = posterAranzmana;
            Smestaj = smestaj;
            Obrisan = obrisan;
        }

        public string Naziv { get => naziv; set => naziv = value; }
        public string TipAranzmana { get => tipAranzmana; set => tipAranzmana = value; }
        public string TipPrevoza { get => tipPrevoza; set => tipPrevoza = value; }
        public string Lokacija { get => lokacija; set => lokacija = value; }
        public DateTime DatumPocetkaPutovanja { get => datumPocetkaPutovanja; set => datumPocetkaPutovanja = value; }
        public DateTime DatumZavrsetkaPutovanja { get => datumZavrsetkaPutovanja; set => datumZavrsetkaPutovanja = value; }
        public MestoNalazenja MestoNalazenja { get => mestoNalazenja; set => mestoNalazenja = value; }
        public TimeSpan VremeNalazenja { get => vremeNalazenja; set => vremeNalazenja = value; }
        public int MaxBrojPutnika { get => maxBrojPutnika; set => maxBrojPutnika = value; }
        public string OpisAranzmana { get => opisAranzmana; set => opisAranzmana = value; }
        public string ProgramPutovanja { get => programPutovanja; set => programPutovanja = value; }
        public string PosterAranzmana { get => posterAranzmana; set => posterAranzmana = value; }
        public Smestaj Smestaj { get => smestaj; set => smestaj = value; }
        public bool Obrisan { get => obrisan; set => obrisan = value; }
    }
}