﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR21_2018_Web_Projekat.Models
{
    public class SmestajnaJedinica
    {
        int brojSmestajneJedinice;
        int dozvoljenBrGostiju;
        bool ljubimci;
        int cena;
        bool zauzeta;
        bool obrisana;

        public SmestajnaJedinica(int br, int dozvoljenBrGostiju, bool ljubimci, int cena, bool zauzeta, bool obrisana)
        {
            BrojSmestajneJedinice = br;
            DozvoljenBrGostiju = dozvoljenBrGostiju;
            Ljubimci = ljubimci;
            Cena = cena;
            Zauzeta = zauzeta;
            Obrisana = obrisana;
        }

        public int DozvoljenBrGostiju { get => dozvoljenBrGostiju; set => dozvoljenBrGostiju = value; }
        public bool Ljubimci { get => ljubimci; set => ljubimci = value; }
        public int Cena { get => cena; set => cena = value; }
        public bool Zauzeta { get => zauzeta; set => zauzeta = value; }
        public int BrojSmestajneJedinice { get => brojSmestajneJedinice; set => brojSmestajneJedinice = value; }
        public bool Obrisana { get => obrisana; set => obrisana = value; }
    }
}