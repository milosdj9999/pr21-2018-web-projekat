﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR21_2018_Web_Projekat.Models
{
    public class Komentar
    {
        string turista;
        string aranzman;
        string tekst;
        int ocena;
        bool vidljiv;

        public Komentar(string turista, string aranzman, string tekst, int ocena, bool vidljiv)
        {
            Turista = turista;
            Aranzman = aranzman;
            Tekst = tekst;
            Ocena = ocena;
            Vidljiv = vidljiv;
        }

        public string Turista { get => turista; set => turista = value; }
        public string Aranzman { get => aranzman; set => aranzman = value; }
        public string Tekst { get => tekst; set => tekst = value; }
        public int Ocena { get => ocena; set => ocena = value; }
        public bool Vidljiv { get => vidljiv; set => vidljiv = value; }
    }
}