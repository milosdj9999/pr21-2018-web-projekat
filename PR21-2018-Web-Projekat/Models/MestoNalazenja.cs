﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR21_2018_Web_Projekat.Models
{
    public class MestoNalazenja
    {
        string adresa;
        double geoDuzina;
        double geoSirina;

        public MestoNalazenja(string adresa, double geoDuzina, double geoSirina)
        {
            Adresa = adresa;
            GeoDuzina = geoDuzina;
            GeoSirina = geoSirina;
        }

        public string Adresa { get => adresa; set => adresa = value; }
        public double GeoDuzina { get => geoDuzina; set => geoDuzina = value; }
        public double GeoSirina { get => geoSirina; set => geoSirina = value; }
    }
}