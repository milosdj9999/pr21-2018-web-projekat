﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR21_2018_Web_Projekat.Models
{
    public class Rezervacija
    {
        string identifikator;
        string turistaKojiRezervise;
        bool status;
        string aranzman;
        SmestajnaJedinica smestajnaJedinica;

        public Rezervacija(string identifikator, string turistaKojiRezervise, bool status, string aranzman, SmestajnaJedinica smestajnaJedinica)
        {
            Identifikator = identifikator;
            TuristaKojiRezervise = turistaKojiRezervise;
            Status = status;
            Aranzman = aranzman;
            SmestajnaJedinica = smestajnaJedinica;
        }

        public string Identifikator { get => identifikator; set => identifikator = value; }
        public string TuristaKojiRezervise { get => turistaKojiRezervise; set => turistaKojiRezervise = value; }
        public bool Status { get => status; set => status = value; }
        public string Aranzman { get => aranzman; set => aranzman = value; }
        public SmestajnaJedinica SmestajnaJedinica { get => smestajnaJedinica; set => smestajnaJedinica = value; }
    }
}