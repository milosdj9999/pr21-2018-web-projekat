﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR21_2018_Web_Projekat.Models
{
    public class Administrator
    {

        string korisnickoIme;
        string lozinka;
        string ime;
        string prezime;
        string pol;
        string email;
        DateTime datumRodjenja;
        EUloga uloga;
        

        public Administrator(string korisnickoIme, string lozinka, string ime, string prezime, string pol, string email, DateTime datumRodjenja, EUloga uloga)
        {
            KorisnickoIme = korisnickoIme;
            Lozinka = lozinka;
            Ime = ime;
            Prezime = prezime;
            Pol = pol;
            Email = email;
            DatumRodjenja = datumRodjenja;
            Uloga = uloga;       
        }

        public string KorisnickoIme { get => korisnickoIme; set => korisnickoIme = value; }
        public string Lozinka { get => lozinka; set => lozinka = value; }
        public string Ime { get => ime; set => ime = value; }
        public string Prezime { get => prezime; set => prezime = value; }
        public string Pol { get => pol; set => pol = value; }
        public string Email { get => email; set => email = value; }
        public DateTime DatumRodjenja { get => datumRodjenja; set => datumRodjenja = value; }
        public EUloga Uloga { get => uloga; set => uloga = value; }
        
    }
}