﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR21_2018_Web_Projekat.Models.Enums
{
    public enum ETipAranzmana
    {
        nocenjeSaDoruckom,
        poluPansion,
        punPansion,
        allInclusive,
        najamApartmana
    }
}