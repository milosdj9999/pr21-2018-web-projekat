﻿using PR21_2018_Web_Projekat.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR21_2018_Web_Projekat.Models
{
    public class Smestaj
    {
        string tipSmestaja;
        string naziv;
        int brojZvezdica;
        bool postojanjeBazena;
        bool postojanjeSpaCentra;
        bool invaliditet;
        bool wifi;
        List<SmestajnaJedinica> smestajneJedinice;
        bool obrisan;

        public Smestaj(string tipSmestaja, string naziv, int brojZvezdica, bool postojanjeBazena, bool postojanjeSpaCentra, bool invaliditet, bool wifi, List<SmestajnaJedinica> smestajneJedinice, bool obrisan)
        {
            TipSmestaja = tipSmestaja;
            Naziv = naziv;
            BrojZvezdica = brojZvezdica;
            PostojanjeBazena = postojanjeBazena;
            PostojanjeSpaCentra = postojanjeSpaCentra;
            Invaliditet = invaliditet;
            Wifi = wifi;
            SmestajneJedinice = smestajneJedinice;
            Obrisan = obrisan;
        }

        public string TipSmestaja { get => tipSmestaja; set => tipSmestaja = value; }
        public string Naziv { get => naziv; set => naziv = value; }
        public int BrojZvezdica { get => brojZvezdica; set => brojZvezdica = value; }
        public bool PostojanjeBazena { get => postojanjeBazena; set => postojanjeBazena = value; }
        public bool PostojanjeSpaCentra { get => postojanjeSpaCentra; set => postojanjeSpaCentra = value; }
        public bool Invaliditet { get => invaliditet; set => invaliditet = value; }
        public bool Wifi { get => wifi; set => wifi = value; }
        public List<SmestajnaJedinica> SmestajneJedinice { get => smestajneJedinice; set => smestajneJedinice = value; }
        public bool Obrisan { get => obrisan; set => obrisan = value; }
    }
}