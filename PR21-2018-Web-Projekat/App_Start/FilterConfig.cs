﻿using System.Web;
using System.Web.Mvc;

namespace PR21_2018_Web_Projekat
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
