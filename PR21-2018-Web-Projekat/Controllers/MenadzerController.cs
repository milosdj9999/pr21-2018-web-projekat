﻿using PR21_2018_Web_Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR21_2018_Web_Projekat.Controllers
{
    public class MenadzerController : Controller
    {

        #region Index
        // GET: Menadzer
        public ActionResult Index()
        {
            ViewBag.Aranzmani = (List<Aranzman>)HttpContext.Application["ARANZMANI"];
            return View("~/Views/Menadzer/MenadzerIndex.cshtml");
        }

        #endregion

        #region Prethodni
        public ActionResult Prethodni()
        {
            HttpContext.Application["PRED/PRETH"] = "prethodni";
            List<Aranzman> lista = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            List<Aranzman> lista2 = new List<Aranzman>();

            foreach (Aranzman a in lista)
            {
                if (a.DatumPocetkaPutovanja <= DateTime.Now)
                {
                    lista2.Add(a);
                }
            }

            lista2 = lista2.OrderBy(i => i.DatumZavrsetkaPutovanja).ToList();

            lista2.Reverse();

            ViewBag.Aranzmani = lista2;

            return View("~/Views/Menadzer/MenadzerIndex.cshtml");
        }

        #endregion

        #region Predstojeci
        public ActionResult Predstojeci()
        {
            HttpContext.Application["PRED/PRETH"] = "predstojeci";

            List<Aranzman> lista = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            List<Aranzman> lista2 = new List<Aranzman>();

            foreach (Aranzman a in lista)
            {
                if (a.DatumPocetkaPutovanja >= DateTime.Now)
                {
                    lista2.Add(a);
                }
            }

            lista2 = lista2.OrderBy(i => i.DatumZavrsetkaPutovanja).ToList();

            lista2.Reverse();

            ViewBag.Aranzmani = lista2;

            return View("~/Views/Menadzer/MenadzerIndex.cshtml");
        }

        #endregion

        #region Detaljnije
        public ActionResult Detaljnije(string prikazi)
        {
            List<Aranzman> lista = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            foreach (Aranzman a in lista)
            {
                if (a.Naziv == prikazi)
                {
                    ViewBag.Aranzman = a;
                    break;
                }
            }

            return View();
        }

        #endregion

        #region Sortiraj

        public ActionResult Sortiraj(string kriterijum, string smer)
        {
            List<Aranzman> listaPocetna = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            List<Aranzman> lista = new List<Aranzman>();

            if ((string)HttpContext.Application["PRED/PRETH"] == "prethodni")
            {
                foreach (Aranzman a in listaPocetna)
                {
                    if (a.DatumZavrsetkaPutovanja <= DateTime.Now)
                    {
                        lista.Add(a);
                    }
                }

            }
            else if ((string)HttpContext.Application["PRED/PRETH"] == "predstojeci")
            {
                foreach (Aranzman a in listaPocetna)
                {
                    if (a.DatumZavrsetkaPutovanja >= DateTime.Now)
                    {
                        lista.Add(a);
                    }
                }
            }
            else
            {
                lista = listaPocetna;
            }

            switch (kriterijum)
            {
                case "Datum pocetka putovanja":
                    lista = lista.OrderBy(i => i.DatumPocetkaPutovanja).ToList();
                    break;

                case "Datum zavrsetka putovanja":
                    lista = lista.OrderBy(i => i.DatumZavrsetkaPutovanja).ToList();
                    break;

                case "Tip prevoza":
                    lista = lista.OrderBy(i => i.TipPrevoza).ToList();
                    break;

                case "Naziv aranzmana":
                    lista = lista.OrderBy(i => i.Naziv).ToList();
                    break;

                case "Cena":
                    lista = lista.OrderBy(i => i.Smestaj.SmestajneJedinice.Min(c => c.Cena)).ToList();
                    break;


            }



            if (smer == "Opadajuce")
            {
                lista.Reverse();
            }

            ViewBag.Aranzmani = lista;

            return View("~/Views/Menadzer/MenadzerIndex.cshtml");
        }

        #endregion

        #region Pretrazi

        public ActionResult Pretrazi(Nullable<DateTime> pocetakOd, Nullable<DateTime> pocetakDo, Nullable<DateTime> zavrsetakOd, Nullable<DateTime> zavrsetakDo, string tipPrevoza, string tipAranzmana, string naziv)
        {
            List<Aranzman> listaPocetna = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            List<Aranzman> lista = new List<Aranzman>();



            if ((string)HttpContext.Application["PRED/PRETH"] == "prethodni")
            {
                foreach (Aranzman a in listaPocetna)
                {
                    if (a.DatumZavrsetkaPutovanja <= DateTime.Now)
                    {
                        lista.Add(a);
                    }
                }

            }
            else if ((string)HttpContext.Application["PRED/PRETH"] == "predstojeci")
            {
                foreach (Aranzman a in listaPocetna)
                {
                    if (a.DatumZavrsetkaPutovanja >= DateTime.Now)
                    {
                        lista.Add(a);
                    }
                }
            }
            else
            {
                lista = listaPocetna;
            }

            if (pocetakOd == null || pocetakDo == null || zavrsetakDo == null || zavrsetakOd == null || tipAranzmana == null || tipPrevoza == null || naziv == null)
            {
                ViewBag.Greska = "Niste uneli sve parametre pretrage!";
                ViewBag.Aranzmani = lista;
                return View("~/Views/Menadzer/MenadzerIndex.cshtml");

            }

            switch (tipPrevoza)
            {
                case "Avion":
                    tipPrevoza = "avion";
                    break;
                case "Autobus":
                    tipPrevoza = "autobus";
                    break;
                case "Autobus + avion":
                    tipPrevoza = "autobusAvion";
                    break;
                case "Individualan":
                    tipPrevoza = "individualan";
                    break;
                case "Ostalo":
                    tipPrevoza = "ostalo";
                    break;
            }

            switch (tipAranzmana)
            {
                case "Nocenje sa doruckom":
                    tipAranzmana = "nocenjeSaDoruckom";
                    break;
                case "Polupansion":
                    tipAranzmana = "poluPansion";
                    break;
                case "Pun pansion":
                    tipAranzmana = "punPansion";
                    break;
                case "All inclusive":
                    tipAranzmana = "allInclusive";
                    break;
                case "Najam apartmana":
                    tipAranzmana = "najamApartmana";
                    break;
            }



            List<Aranzman> listaKonacna = new List<Aranzman>();




            foreach (Aranzman a in lista)
            {
                if (a.DatumPocetkaPutovanja >= pocetakOd && a.DatumPocetkaPutovanja <= pocetakDo && a.DatumZavrsetkaPutovanja >= zavrsetakOd && a.DatumZavrsetkaPutovanja <= zavrsetakDo && a.TipAranzmana == tipAranzmana && a.TipPrevoza == tipPrevoza && a.Naziv.Contains(naziv))
                {
                    listaKonacna.Add(a);
                }
            }

            ViewBag.Aranzmani = listaKonacna;
            return View("~/Views/Menadzer/MenadzerIndex.cshtml");
        }

        #endregion

        #region MojiAranzmani

        public ActionResult MojiAranzmani()
        {
            ViewBag.Aranzmani = ((Menadzer)HttpContext.Session["USER"]).Aranzmani;

            return View();
        }

        #endregion

        #region DodajAranzman

        public ActionResult DodajAranzman()
        {
            return View();
        }

        #endregion

        #region PotvrdiDodavanjeAranzmana

        public ActionResult PotvrdiDodavanjeAranzmana(string naziv, string tipAranzmana, string tipPrevoza, string lokacija, Nullable<DateTime> datumPocetkaPutovanja, Nullable<DateTime> datumZavrsetkaPutovanja, string adresa, string geoDuzina, string geoSirina, Nullable<DateTime> vreme, string maxBrPutnika, string opisAranzmana, string programPutovanja, string poster)
        {

            if (naziv == null || tipAranzmana == null || tipPrevoza == null || lokacija == null || datumPocetkaPutovanja == null || datumZavrsetkaPutovanja == null || adresa == null || geoDuzina == null || geoSirina == null || vreme == null || maxBrPutnika == null || opisAranzmana == null || programPutovanja == null || poster == null)
            {
                ViewBag.Greska = "Niste uneli sve podatke!";
                return View("~/Views/Menadzer/DodajAranzman.cshtml");
            }

            double g;
            int gg;

            if (Double.TryParse(geoDuzina, out g)==false || Double.TryParse(geoSirina, out g)==false || Int32.TryParse(maxBrPutnika, out gg)==false)
            {
                ViewBag.Greska = "Niste dobro uneli geografsku sirinu/duzinu/maksimalan broj putnika!";
                return View("~/Views/Menadzer/DodajAranzman.cshtml");
            }

            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            foreach(Aranzman a in aranzmani)
            {
                if(a.Naziv == naziv)
                {
                    ViewBag.Greska = "Aranzman sa tim nazivom vec postoji!";
                    return View("~/Views/Menadzer/DodajAranzman.cshtml");
                }
            }

            MestoNalazenja mesto = new MestoNalazenja(adresa, Double.Parse(geoDuzina), Double.Parse(geoSirina));

            TimeSpan vremeTimeSpan = TimeSpan.FromTicks(vreme.Value.Ticks);

            DateTime datumPocetkaPutovanjaa = (DateTime)datumPocetkaPutovanja;
            DateTime datumZavrsetkaPutovanjaa = (DateTime)datumZavrsetkaPutovanja;

            Aranzman novi = new Aranzman(naziv, tipAranzmana, tipPrevoza, lokacija, datumPocetkaPutovanjaa, datumZavrsetkaPutovanjaa, mesto, vremeTimeSpan, Int32.Parse(maxBrPutnika), opisAranzmana, programPutovanja, poster, null, false);

            aranzmani.Add(novi);

            HttpContext.Application["ARANZMANI"] = aranzmani;

            Menadzer m = (Menadzer)HttpContext.Session["USER"];

            List<Menadzer> menadzeri = (List<Menadzer>)HttpContext.Application["MENADZERI"];


            foreach(Menadzer mr in menadzeri)
            {
                if(mr.KorisnickoIme == m.KorisnickoIme)
                {
                    mr.Aranzmani.Add(novi);
                }
            }

            HttpContext.Application["MENADZERI"] = menadzeri;


            ViewBag.Aranzmani = m.Aranzmani;
            ViewBag.Poruka = $"Uspesno dodavanje aranzmana {naziv}";
            return View("~/Views/Menadzer/MojiAranzmani.cshtml");
        }

        #endregion      

        #region ObrisiAranzman

        public ActionResult ObrisiAranzman(string prikazi)
        {
            string naziv = prikazi;

            List<Turista> turisti = (List<Turista>)HttpContext.Application["TURISTI"];


            foreach (Turista t in turisti)
            {
                foreach (Rezervacija r in t.Rezervacije)
                {
                    if (naziv == r.Aranzman)
                    {
                        ViewBag.Poruka = "Za ovaj aranzman vec postoji rezervacija, nije dozvoljeno brisanje!";
                        ViewBag.Aranzmani = ((Menadzer)HttpContext.Session["USER"]).Aranzmani;
                        return View("~/Views/Menadzer/MojiAranzmani.cshtml");
                    }
                }
            }




            Menadzer m = (Menadzer)HttpContext.Session["USER"];


            foreach (Aranzman a in m.Aranzmani)
            {
                if (a.Naziv == naziv)
                {
                    a.Obrisan = true;
                }
            }

            HttpContext.Session["USER"] = m;

            List<Menadzer> menadzeri = (List<Menadzer>)HttpContext.Application["MENADZERI"];

            foreach(Menadzer mm in menadzeri)
            {
                if (mm.Ime == m.Ime)
                {
                    foreach(Aranzman ar in mm.Aranzmani)
                    {
                        if (ar.Naziv == naziv)
                        {
                            ar.Obrisan = true;
                        }
                    }
                }
            }

            HttpContext.Application["MENADZERI"] = menadzeri;

            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            foreach (Aranzman ar in aranzmani)
            {
                if (ar.Naziv == naziv)
                {
                    ar.Obrisan = true;
                }
            }

            HttpContext.Application["ARANZMANI"] = aranzmani;

            ViewBag.Poruka = $"Aranzman {naziv} je uspesno obrisan!";
            ViewBag.Aranzmani = m.Aranzmani;

            return View("~/Views/Menadzer/MojiAranzmani.cshtml");
        }

        #endregion

        #region IzmeniAranzman

        public ActionResult IzmeniAranzman(string prikazi)
        {

            Menadzer m = (Menadzer)HttpContext.Session["USER"];
            List<Aranzman> lista = m.Aranzmani;

            Aranzman ar = null;

            foreach (Aranzman a in lista) { 

                if(a.Naziv == prikazi)
                {
                    ar = a;
                }
            }


            ViewBag.Aranzman = ar;
            HttpContext.Session["ARANZMAN"] = ar;
            return View();
        }

        #endregion

        #region PotvrdiIzmenuAranzmana

        public ActionResult PotvrdiIzmenuAranzmana(string nazivZaIzmenu, string naziv, string tipAranzmana, string tipPrevoza, string lokacija, Nullable<DateTime> datumPocetkaPutovanja, Nullable<DateTime> datumZavrsetkaPutovanja, string adresa, string geoDuzina, string geoSirina, Nullable<DateTime> vreme, string maxBrPutnika, string opisAranzmana, string programPutovanja, string poster)
        {

            if (naziv == null || tipAranzmana == null || tipPrevoza == null || lokacija == null || datumPocetkaPutovanja == null || datumZavrsetkaPutovanja == null || adresa == null || geoDuzina == null || geoSirina == null || vreme == null || maxBrPutnika == null || opisAranzmana == null || programPutovanja == null || poster == null)
            {
                ViewBag.Greska = "Niste uneli sve podatke!";
                ViewBag.Aranzman = (Aranzman)HttpContext.Session["ARANZMAN"];
                return View("~/Views/Menadzer/IzmeniAranzman.cshtml");
            }

            double g;
            int gg;

            
            if (Double.TryParse(geoDuzina, out g)==false || Double.TryParse(geoSirina, out g)==false || Int32.TryParse(maxBrPutnika, out gg)==false)
            {
                ViewBag.Greska = "Niste dobro uneli geografsku sirinu/duzinu/maksimalan broj putnika!";
                ViewBag.Aranzman = (Aranzman)HttpContext.Session["ARANZMAN"];

                return View("~/Views/Menadzer/IzmeniAranzman.cshtml");
            }

            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            Aranzman novi = null;

            foreach(Aranzman a in aranzmani)
            {
                if(a.Naziv == nazivZaIzmenu)
                {
                    novi = a;
                }
            }

            MestoNalazenja mesto = new MestoNalazenja(adresa, Double.Parse(geoDuzina), Double.Parse(geoSirina));

            TimeSpan vremeTimeSpan = TimeSpan.FromTicks(vreme.Value.Ticks);

            
            List<SmestajnaJedinica> sj = new List<SmestajnaJedinica>();

            DateTime datumPocetkaPutovanjaa = (DateTime)datumPocetkaPutovanja;
            DateTime datumZavrsetkaPutovanjaa = (DateTime)datumZavrsetkaPutovanja;

            novi.Naziv = naziv;
            novi.TipAranzmana = tipAranzmana;
            novi.TipPrevoza = tipPrevoza;
            novi.Lokacija = lokacija;
            novi.DatumPocetkaPutovanja = datumPocetkaPutovanjaa;
            novi.DatumZavrsetkaPutovanja = datumZavrsetkaPutovanjaa;
            novi.MestoNalazenja = mesto;
            novi.VremeNalazenja = vremeTimeSpan;
            novi.MaxBrojPutnika = Int32.Parse(maxBrPutnika);
            novi.OpisAranzmana = opisAranzmana;
            novi.ProgramPutovanja = programPutovanja;
            novi.PosterAranzmana = poster;
            

            

            foreach (Aranzman a in aranzmani)
            {
                if (a.Naziv == nazivZaIzmenu)
                {
                    aranzmani.Remove(a);
                    break;
                }
            }
            aranzmani.Add(novi);

            HttpContext.Application["ARANZMANI"] = aranzmani;

            Menadzer m = (Menadzer)HttpContext.Session["USER"];
            
            List<Menadzer> menadzeri = (List<Menadzer>)HttpContext.Application["MENADZERI"];

            Menadzer menadzerZaIzmenu = null;

            foreach(Menadzer mena in menadzeri)
            {
                if (mena.KorisnickoIme == m.KorisnickoIme)
                {
                    menadzerZaIzmenu = mena;
                }
            }

            menadzeri.Remove(menadzerZaIzmenu);
            menadzeri.Add(m);

            HttpContext.Application["MENADZERI"] = menadzeri;

            ViewBag.Poruka = $"Uspesno ste izmenili aranzman {nazivZaIzmenu}";
            ViewBag.Aranzmani = ((Menadzer)HttpContext.Session["USER"]).Aranzmani;

            return View("~/Views/Menadzer/MojiAranzmani.cshtml");
        }

        #endregion

        #region MojiSmestaji

        public ActionResult MojiSmestaji()
        {


            Menadzer m = (Menadzer)HttpContext.Session["USER"];

            List<Aranzman> aranzmani = m.Aranzmani;

            List <Smestaj> smestaji = new List<Smestaj>();

            foreach(Aranzman a in aranzmani)
            {
                smestaji.Add(a.Smestaj);
            }

            ViewBag.Smestaji = smestaji;

            return View();
        }

        #endregion

        #region DodajSmestaj

        public ActionResult DodajSmestaj()
        {
            Menadzer m = (Menadzer)HttpContext.Session["USER"];

            ViewBag.MojiAranzmani = m.Aranzmani;
            
            return View();
        }

        #endregion

        #region PotvrdiDodavanjeSmestaja

        public ActionResult PotvrdiDodavanjeSmestaja(string nazivAranzmana, string tipSmestaja, string nazivS, string brojZvezdica, string postojanjeBazena, string postojanjeSpaCentra, string invaliditet, string wifi)
        {
            if (nazivAranzmana == null || tipSmestaja == null || nazivS == null || brojZvezdica == null || postojanjeBazena == null || postojanjeSpaCentra == null || invaliditet == null || wifi == null)
            {
                ViewBag.Greska = "Niste uneli sve podatke!";
                return View("~/Views/Menadzer/DodajSmestaj");
            }


            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["ARANZMANI"];
                       
            bool bazenBool = (postojanjeBazena == "Da") ? true : false;
            bool spaBool = (postojanjeSpaCentra == "Da") ? true : false;
            bool invaliditetBool = (invaliditet == "Da") ? true : false;
            bool wifiBool = (wifi == "Da") ? true : false;

            Smestaj s = new Smestaj(tipSmestaja, nazivS, Int32.Parse(brojZvezdica), bazenBool, spaBool, invaliditetBool, wifiBool, new List<SmestajnaJedinica>(), false);

            foreach(Aranzman a in aranzmani)
            {
                if(a.Naziv == nazivAranzmana)
                {
                    a.Smestaj = s;
                }
            }

            Menadzer m = (Menadzer)HttpContext.Session["USER"];

            List<Menadzer> menadzeri = (List<Menadzer>)HttpContext.Application["MENADZERI"];

            foreach(Menadzer mr in menadzeri)
            {
                if(mr.KorisnickoIme == m.KorisnickoIme)
                {
                    foreach(Aranzman ar in mr.Aranzmani)
                    {
                        if(ar.Naziv == nazivAranzmana)
                        {
                            ar.Smestaj = s;
                        }
                    }
                }
            }

            ViewBag.Poruka = $"Smestaj {nazivS} uspesno dodat!";

            List<Smestaj> smestaji = new List<Smestaj>();

            foreach(Aranzman arr in m.Aranzmani)
            {
                smestaji.Add(arr.Smestaj);
            }

            ViewBag.Smestaji = smestaji;
            return View("~/Views/Menadzer/MojiSmestaji.cshtml");
        }

        #endregion

        #region ObrisiSmestaj

        public ActionResult ObrisiSmestaj(string naziv)
        {
            Menadzer m = (Menadzer)HttpContext.Session["USER"];

            List<Smestaj> smestaji = new List<Smestaj>();

            foreach(Aranzman ar in m.Aranzmani)
            {
                smestaji.Add(ar.Smestaj);
            }

            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            foreach(Aranzman a in aranzmani)
            {
                if(a.Smestaj.Naziv==naziv && a.DatumPocetkaPutovanja >= DateTime.Now)
                {
                    ViewBag.Poruka = $"Nije moguce obrisati smestaj {naziv}, jer postoji aranzman sa tim smestajem u buducnosti!";
                    ViewBag.Smestaji = smestaji;
                    return View("~/Views/Menadzer/MojiSmestaji.cshtml");
                }
            }

            foreach(Aranzman a in aranzmani)
            {
                if (a.Smestaj.Naziv == naziv)
                {
                    a.Smestaj.Obrisan = true;
                }
            }

            HttpContext.Application["ARANZMANI"] = aranzmani;


            List<Menadzer> menadzeri = (List<Menadzer>)HttpContext.Application["MENADZERI"];

            foreach(Menadzer mr in menadzeri)
            {
                if(mr.KorisnickoIme == m.KorisnickoIme)
                {
                    foreach(Aranzman ar in mr.Aranzmani)
                    {
                        if (ar.Smestaj.Naziv == naziv)
                        {
                            ar.Smestaj.Obrisan = true;
                        }
                    }
                }
            }
            HttpContext.Application["MENADZERI"] = menadzeri;

            smestaji.Clear();

            foreach (Aranzman ar in m.Aranzmani)
            {
                smestaji.Add(ar.Smestaj);
            }
            ViewBag.Smestaji = smestaji;

            ViewBag.Poruka = $"Smestaj {naziv} uspesno obrisan!";
            return View("~/Views/Menadzer/MojiSmestaji.cshtml");


        }


        #endregion

        #region IzmeniSmestaj

        public ActionResult IzmeniSmestaj(string naziv)
        {
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            foreach(Aranzman a in aranzmani)
            {
                if (a.Smestaj.Naziv == naziv)
                {
                    ViewBag.Smestaj = a.Smestaj;
                    HttpContext.Application["SMESTAJ"] = a.Smestaj;
                    break;
                }
            }
            return View();
        }

        #endregion

        #region PotvrdiIzmenuSmestaja

        public ActionResult PotvrdiIzmenuSmestaja(string stariNaziv, string tipSmestaja, string nazivS, string brojZvezdica, string postojanjeBazena, string postojanjeSpaCentra, string invaliditet, string wifi)
        {
            if (tipSmestaja == null || nazivS == null || brojZvezdica == null || postojanjeBazena == null || postojanjeSpaCentra == null || invaliditet == null || wifi == null)
            {
                ViewBag.Greska = "Niste uneli sve podatke!";
                ViewBag.Smestaj = (Smestaj)HttpContext.Application["SMESTAJ"];
                return View("~/Views/Menadzer/IzmeniSmestaj.cshtml");
            }

            bool bazenBool = (postojanjeBazena == "Da") ? true : false;
            bool spaBool = (postojanjeSpaCentra == "Da") ? true : false;
            bool invaliditetBool = (invaliditet == "Da") ? true : false;
            bool wifiBool = (wifi == "Da") ? true : false;

            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            foreach (Aranzman a in aranzmani)
            {
                if (a.Smestaj.Naziv == stariNaziv)
                {

                    a.Smestaj.TipSmestaja = tipSmestaja;
                    a.Smestaj.Naziv = nazivS;
                    a.Smestaj.BrojZvezdica = Int32.Parse(brojZvezdica);
                    a.Smestaj.PostojanjeBazena = bazenBool;
                    a.Smestaj.PostojanjeSpaCentra = spaBool;
                    a.Smestaj.Invaliditet = invaliditetBool;
                    a.Smestaj.Wifi = wifiBool;

                }
            }

            HttpContext.Application["ARANZMANI"] = aranzmani;

            Menadzer m = (Menadzer)HttpContext.Session["USER"];

            List<Menadzer> menadzeri = (List<Menadzer>)HttpContext.Application["MENADZERI"];

            foreach (Menadzer mr in menadzeri)
            {
                if (mr.KorisnickoIme == m.KorisnickoIme)
                {
                    foreach (Aranzman a in mr.Aranzmani)
                    {
                        if (a.Smestaj.Naziv == stariNaziv)
                        {

                            a.Smestaj.TipSmestaja = tipSmestaja;
                            a.Smestaj.Naziv = nazivS;
                            a.Smestaj.BrojZvezdica = Int32.Parse(brojZvezdica);
                            a.Smestaj.PostojanjeBazena = bazenBool;
                            a.Smestaj.PostojanjeSpaCentra = spaBool;
                            a.Smestaj.Invaliditet = invaliditetBool;
                            a.Smestaj.Wifi = wifiBool;

                        }
                    }
                }
            }

            HttpContext.Application["MENADZERI"] = menadzeri;

            List<Smestaj> smestaji = new List<Smestaj>();

            foreach (Aranzman ar in m.Aranzmani)
            {
                smestaji.Add(ar.Smestaj);
            }
            ViewBag.Smestaji = smestaji;


            ViewBag.Greska = $"Smestaj {nazivS} uspesno izmenjen!";
            return View("~/Views/Menadzer/MojiSmestaji.cshtml");

        }

        #endregion        

        #region IzmeniSmestajnuJedinicu

        public ActionResult IzmeniSmestajnuJedinicu(string nazivSmestaja, string brJedinice)
        {

            Menadzer m = (Menadzer)HttpContext.Session["USER"];

            foreach(Aranzman a in m.Aranzmani)
            {
                if(a.Smestaj.Naziv == nazivSmestaja)
                {
                    foreach(SmestajnaJedinica sj in a.Smestaj.SmestajneJedinice)
                    {
                        if (sj.BrojSmestajneJedinice == Int32.Parse(brJedinice))
                        {
                            ViewBag.SmestajnaJedinica = sj;
                            HttpContext.Session["SJ"] = sj;
                        }
                    }
                }
            }

            return View();
        }

        #endregion
               
        #region PotvrdiIzmenuSmestajneJedinice

        public ActionResult PotvrdiIzmenuSmestajneJedinice(string stariBR, string brGostiju, string cena, string ljubimci)
        {
            
            
            if(brGostiju==null || cena == null || ljubimci == null)
            {
                ViewBag.SmestajnaJedinica = (SmestajnaJedinica)HttpContext.Session["SJ"];
                ViewBag.Greska = "Niste uneli sve podatke!";
                return View("~/Views/Menadzer/IzmeniSmestajnuJedinicu.cshtml");
            }

            int g;

            if (Int32.TryParse(brGostiju, out g) == false || Int32.TryParse(cena, out g) == false)
            {
                ViewBag.SmestajnaJedinica = (SmestajnaJedinica)HttpContext.Session["SJ"];
                ViewBag.Greska = "Niste dobro uneli podatke!";
                return View("~/Views/Menadzer/IzmeniSmestajnuJedinicu.cshtml");
            }


            bool ljubimciBool = (ljubimci == "Da") ? true : false;

            Smestaj sm = (Smestaj)HttpContext.Session["SMESTAJ"];

            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            foreach(Aranzman a in aranzmani)
            {
                if (a.Smestaj.Naziv == sm.Naziv)
                {
                    foreach (SmestajnaJedinica sj in a.Smestaj.SmestajneJedinice)
                    {
                        if (sj.BrojSmestajneJedinice == Int32.Parse(stariBR))
                        {
                            sj.DozvoljenBrGostiju = Int32.Parse(brGostiju);
                            sj.Cena = Int32.Parse(cena);
                            sj.Ljubimci = ljubimciBool;
                            HttpContext.Session["SMESTAJ"] = a.Smestaj;
                            break;
                        }
                    }
                }
            }

            HttpContext.Application["ARANZMANI"] = aranzmani;

            List<Menadzer> menadzeri = (List<Menadzer>)HttpContext.Application["MENADZERI"];

            Menadzer m = (Menadzer)HttpContext.Session["USER"];

            foreach (Menadzer mr in menadzeri)
            {
                if (mr.KorisnickoIme == m.KorisnickoIme)
                {
                    foreach (Aranzman a in mr.Aranzmani)
                    {
                        foreach (SmestajnaJedinica sj in a.Smestaj.SmestajneJedinice)
                        {
                            if (sj.BrojSmestajneJedinice == Int32.Parse(stariBR))
                            {
                                sj.DozvoljenBrGostiju = Int32.Parse(brGostiju);
                                sj.Cena = Int32.Parse(cena);
                                sj.Ljubimci = ljubimciBool;                                
                            }
                        }
                    }
                }
            }

            HttpContext.Application["MENADZERI"] = menadzeri;

            Menadzer me = (Menadzer)HttpContext.Session["USER"];

            foreach (Aranzman a in me.Aranzmani)
            {
                foreach (SmestajnaJedinica sj in a.Smestaj.SmestajneJedinice)
                {
                    if (sj.BrojSmestajneJedinice == Int32.Parse(stariBR))
                    {
                        sj.DozvoljenBrGostiju = Int32.Parse(brGostiju);
                        sj.Cena = Int32.Parse(cena);
                        sj.Ljubimci = ljubimciBool;

                    }
                }
            }
            HttpContext.Session["USER"] = me;
            
            ViewBag.Smestaj = (Smestaj)HttpContext.Session["SMESTAJ"];
            ViewBag.Poruka = "Smestajna jedinica uspesno izmenjena!";
            return View("~/Views/Menadzer/Smestaj.cshtml");


        }


        #endregion

        #region ObrisiSmestajnuJedinicu

        public ActionResult ObrisiSmestajnuJedinicu(string nazivSmestaja, string brJedinice)
        {


            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["ARANZMANI"];




            foreach (Aranzman a in aranzmani)
            {
                if (a.Smestaj.Naziv == nazivSmestaja)
                {
                    foreach (SmestajnaJedinica sj in a.Smestaj.SmestajneJedinice)
                    {
                        if (sj.BrojSmestajneJedinice == Int32.Parse(brJedinice))
                        {
                            sj.Obrisana = true;
                            break;
                        }
                    }
                }
            }

            HttpContext.Application["ARANZMANI"] = aranzmani;

            List<Menadzer> menadzeri = (List<Menadzer>)HttpContext.Application["MENADZERI"];

            Menadzer m = (Menadzer)HttpContext.Session["USER"];

            foreach (Menadzer mr in menadzeri)
            {
                if (mr.KorisnickoIme == m.KorisnickoIme)
                {
                    foreach (Aranzman a in mr.Aranzmani)
                    {
                        if (a.Smestaj.Naziv == nazivSmestaja)
                        {
                            foreach (SmestajnaJedinica sj in a.Smestaj.SmestajneJedinice)
                            {
                                if (sj.BrojSmestajneJedinice == Int32.Parse(brJedinice))
                                {
                                    sj.Obrisana = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            HttpContext.Application["MENADZERI"] = menadzeri;

            foreach (Aranzman a in m.Aranzmani)
            {
                if (a.Smestaj.Naziv == nazivSmestaja)
                {
                    foreach (SmestajnaJedinica sj in a.Smestaj.SmestajneJedinice)
                    {
                        if (sj.BrojSmestajneJedinice == Int32.Parse(brJedinice))
                        {
                            sj.Obrisana = true;
                            break;

                        }
                    }
                }
            }
            HttpContext.Session["USER"] = m;


            ViewBag.Smestaj = (Smestaj)HttpContext.Session["SMESTAJ"];
            ViewBag.Poruka = "Smestajna jedinica uspesno obrisana!";
            return View("~/Views/Menadzer/Smestaj.cshtml");
        }


        #endregion

        #region DodajSmestajnuJedinicu

        public ActionResult DodajSmestajnuJedinicu()
        {
            return View();
        }


        #endregion

        #region PotvrdiDodavanjeSmestajneJedinice

        public ActionResult PotvrdiDodavanjeSmestajneJedinice(string brSJ, string brGostiju, string cena, string ljubimci)
        {

            string nazivSmestaja = ((Smestaj)HttpContext.Session["SMESTAJ"]).Naziv;


            if(brSJ==null || brGostiju==null || cena == null || ljubimci==null)
            {
                ViewBag.Greska = "Niste uneli sve parametre!";
                return View("~/Views/Menadzer/DodajSmestajnuJedinicu");
            }

            int g;

            if(Int32.TryParse(brSJ, out g) == false || Int32.TryParse(brGostiju, out g) == false || Int32.TryParse(cena, out g) == false)
            {
                ViewBag.Greska = "Niste dobro uneli parametre!";
                return View("~/Views/Menadzer/DodajSmestajnuJedinicu.cshtml");
            }

            bool ljubimciBool = (ljubimci == "Da") ? true : false;

            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            foreach(Aranzman a in aranzmani)
            {
                if(a.Smestaj.Naziv == nazivSmestaja)
                {
                    foreach(SmestajnaJedinica sj in a.Smestaj.SmestajneJedinice)
                    {
                        if(sj.BrojSmestajneJedinice == Int32.Parse(brSJ))
                        {
                            ViewBag.Greska = "Vec postoji smestajna jedinica sa tim brojem!";
                            return View("~/Views/Menadzer/DodajSmestajnuJedinicu.cshtml");
                        }

                        
                    }

                    SmestajnaJedinica nova = new SmestajnaJedinica(Int32.Parse(brSJ), Int32.Parse(brGostiju), ljubimciBool, Int32.Parse(cena), false, false);
                    a.Smestaj.SmestajneJedinice.Add(nova);
                    break;
                }
            }

            HttpContext.Application["ARANZMANI"] = aranzmani;

            /*
            Menadzer m = (Menadzer)HttpContext.Session["USER"];

            List<Menadzer> menadzeri = (List<Menadzer>)HttpContext.Application["MENADZERI"];

            foreach(Menadzer mr in menadzeri)
            {
                if (mr.KorisnickoIme == m.KorisnickoIme)
                {
                    foreach (Aranzman a in mr.Aranzmani)
                    {
                        if (a.Smestaj.Naziv == nazivSmestaja)
                        {
                            foreach (SmestajnaJedinica sj in a.Smestaj.SmestajneJedinice)
                            {
                                if (sj.BrojSmestajneJedinice == Int32.Parse(brSJ))
                                {
                                    ViewBag.Greska = "Vec postoji smestajna jedinica sa tim brojem!";
                                    return View("~/Views/Menadzer/DodajSmestajnuJedinicu.cshtml");
                                }


                            }

                            SmestajnaJedinica nova = new SmestajnaJedinica(Int32.Parse(brSJ), Int32.Parse(brGostiju), ljubimciBool, Int32.Parse(cena), false, false);
                            a.Smestaj.SmestajneJedinice.Add(nova);
                            break;
                        }
                    }
                }
            } 

            HttpContext.Application["MENADZERI"] = menadzeri;

            /*
            foreach (Aranzman a in m.Aranzmani)
            {
                if (a.Smestaj.Naziv == nazivSmestaja)
                {
                    foreach (SmestajnaJedinica sj in a.Smestaj.SmestajneJedinice)
                    {
                        if (sj.BrojSmestajneJedinice == Int32.Parse(brSJ))
                        {
                            ViewBag.Greska = "Vec postoji smestajna jedinica sa tim brojem!";
                            return View("~/Views/Menadzer/DodajSmestajnuJedinicu.cshtml");
                        }


                    }

                    SmestajnaJedinica nova = new SmestajnaJedinica(Int32.Parse(brSJ), Int32.Parse(brGostiju), ljubimciBool, Int32.Parse(cena), false, false);
                    a.Smestaj.SmestajneJedinice.Add(nova);
                    break;
                }
            }

            HttpContext.Session["USER"] = m;
            */

            ViewBag.Poruka = "Uspesno dodata smestajna jedinica!";


            ViewBag.Smestaj = (Smestaj)HttpContext.Session["SMESTAJ"];            
            return View("~/Views/Menadzer/Smestaj.cshtml");

        }

        #endregion

        #region SmestajSamoPregled

        public ActionResult SmestajSamoPregled(string prikazi)
        {
            if(prikazi == null)
            {
                prikazi = ((Aranzman)HttpContext.Session["ARANZMAN"]).Naziv;
            }

            List<Aranzman> lista = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            foreach (Aranzman a in lista)
            {
                if (a.Naziv == prikazi)
                {
                    HttpContext.Session["ARANZMAN"] = a;
                    HttpContext.Session["SMESTAJ"] = a.Smestaj;
                    ViewBag.Smestaj = a.Smestaj;
                    ViewBag.SmestajneJedinice = a.Smestaj.SmestajneJedinice;
                    break;
                }
            }
            return View("~/Views/Menadzer/SmestajSamoPregled.cshtml");
        }

        #endregion

        #region Smestaj

        public ActionResult Smestaj(string naziv)
        {
            List<Aranzman> lista = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            foreach (Aranzman a in lista)
            {
                if (a.Smestaj.Naziv == naziv)
                {
                    
                    HttpContext.Session["ARANZMAN"] = a;
                    HttpContext.Session["SMESTAJ"] = a.Smestaj;
                    ViewBag.Smestaj = a.Smestaj;
                    ViewBag.SmestajneJedinice = a.Smestaj.SmestajneJedinice;
                    break;
                }
            }
            return View("~/Views/Menadzer/Smestaj.cshtml");
        }

        #endregion

        #region SmestajPretraga

        public ActionResult SmestajPretraga(string donja, string gornja, string ljubimci, string cenadonja, string cenagornja)
        {

            if (donja == null || gornja == null || ljubimci == null || cenadonja == null || cenagornja == null)
            {
                ViewBag.Greska = "Niste uneli sve podatke!";
                return View("~/Views/Menadzer/SmestajSamoPregled.cshtml");
            }

            int g;
            if (!Int32.TryParse(donja, out g) || !Int32.TryParse(gornja, out g) || !Int32.TryParse(cenadonja, out g) || !Int32.TryParse(cenagornja, out g))
            {
                ViewBag.Greska = "Niste dobro uneli podatke!";
                return View("~/Views/Menadzer/SmestajSamoPregled.cshtml");
            }

            Smestaj s = (Smestaj)HttpContext.Session["SMESTAJ"];

            List<SmestajnaJedinica> lista = new List<SmestajnaJedinica>();

            bool ljubimciBool = ((ljubimci == "Da") ? true : false);

            foreach (SmestajnaJedinica sj in s.SmestajneJedinice)
            {
                if (sj.DozvoljenBrGostiju >= Int32.Parse(donja) && sj.DozvoljenBrGostiju <= Int32.Parse(gornja) && sj.Cena >= Int32.Parse(cenadonja) && sj.Cena <= Int32.Parse(cenagornja) && sj.Ljubimci == ljubimciBool)
                {
                    lista.Add(sj);
                }
            }

            ViewBag.SmestajneJedinice = lista;
            ViewBag.Smestaj = s;

            return View("~/Views/Menadzer/SmestajSamoPregled.cshtml");
        }

        #endregion

        #region SmestajSortiranje


        public ActionResult SmestajSortiranje(string kriterijum, string smer)
        {

            Smestaj s = (Smestaj)HttpContext.Session["SMESTAJ"];
            List<SmestajnaJedinica> lista = s.SmestajneJedinice;

            switch (kriterijum)
            {
                case "Dozvoljen broj gostiju":
                    lista = lista.OrderBy(i => i.DozvoljenBrGostiju).ToList();
                    break;

                case "Cena":
                    lista = lista.OrderBy(i => i.Cena).ToList();
                    break;

            }



            if (smer == "Opadajuce")
            {
                lista.Reverse();
            }

            ViewBag.SmestajneJedinice = lista;
            ViewBag.Smestaj = s;
            return View("~/Views/Menadzer/SmestajSamoPregled.cshtml");
        }

        #endregion

        #region PrikaziKomentare

        public ActionResult PrikaziKomentare(string naziv)
        {
            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["KOMENTARI"];

            List<Komentar> lista = new List<Komentar>();

            foreach (Komentar k in komentari)
            {
                if (k.Aranzman == naziv && k.Vidljiv == true)
                {
                    lista.Add(k);
                }
            }

            ViewBag.Komentari = lista;

            return View("~/Views/Home/PrikaziKomentare.cshtml");

        }

        #endregion

        #region SviKomentari

        public ActionResult SviKomentari()
        {
            List<Komentar> lista = (List<Komentar>)HttpContext.Application["KOMENTARI"];

            ViewBag.Komentari = lista;

            return View();
        }

        #endregion

        #region Odobri

        public ActionResult Odobri(string turista, string komentar, string ocena){

            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["KOMENTARI"];

            foreach(Komentar k in komentari)
            {
                if(k.Turista == turista && k.Tekst == komentar && k.Ocena == Int32.Parse(ocena))
                {
                    k.Vidljiv = true;
                    
                }
            }

            HttpContext.Application["KOMENTARI"] = komentari;

            ViewBag.Komentari = komentari;

            return View("~/Views/Menadzer/SviKomentari.cshtml");



        }



        #endregion

    }
}