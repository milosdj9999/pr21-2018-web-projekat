﻿using PR21_2018_Web_Projekat.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;

namespace PR21_2018_Web_Projekat.Controllers
{
    public class HomeController : Controller
    {

        #region Index
        public ActionResult Index()
        {
            HttpContext.Application["PRED/PRETH"] = "svi";

            List<Aranzman> lista = (List<Aranzman>)HttpContext.Application["ARANZMANI"];           

            lista = lista.OrderBy(i => i.DatumZavrsetkaPutovanja).ToList();

            lista.Reverse();

            ViewBag.Aranzmani = lista;

            return View();
        }

        #endregion


        #region Prethodni
        public ActionResult Prethodni()
        {
            HttpContext.Application["PRED/PRETH"] = "prethodni";
            List<Aranzman> lista = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            List<Aranzman> lista2 = new List<Aranzman>();

            foreach (Aranzman a in lista)
            {
                if (a.DatumPocetkaPutovanja <= DateTime.Now)
                {
                    lista2.Add(a);
                }
            }

            lista2 = lista2.OrderBy(i => i.DatumZavrsetkaPutovanja).ToList();

            lista2.Reverse();

            ViewBag.Aranzmani = lista2;

            return View("~/Views/Home/Index.cshtml");
        }

        #endregion

        #region Predstojeci
        public ActionResult Predstojeci()
        {
            HttpContext.Application["PRED/PRETH"] = "predstojeci";

            List<Aranzman> lista = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            List<Aranzman> lista2 = new List<Aranzman>();

            foreach (Aranzman a in lista)
            {
                if (a.DatumPocetkaPutovanja >= DateTime.Now)
                {
                    lista2.Add(a);
                }
            }

            lista2 = lista2.OrderBy(i => i.DatumZavrsetkaPutovanja).ToList();

            lista2.Reverse();

            ViewBag.Aranzmani = lista2;

            return View("~/Views/Home/Index.cshtml");
        }

        #endregion

        #region Detaljnije
        public ActionResult Detaljnije(string prikazi)
        {
            List<Aranzman> lista = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            foreach (Aranzman a in lista)
            {
                if (a.Naziv == prikazi)
                {
                    ViewBag.Aranzman = a;
                    break;
                }
            }

            return View();
        }

        #endregion


        #region Sortiraj

        public ActionResult Sortiraj(string kriterijum, string smer)
        {
            List<Aranzman> listaPocetna = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            List<Aranzman> lista = new List<Aranzman>();

            if ((string)HttpContext.Application["PRED/PRETH"] == "prethodni")
            {
                foreach (Aranzman a in listaPocetna)
                {
                    if (a.DatumZavrsetkaPutovanja <= DateTime.Now)
                    {
                        lista.Add(a);
                    }
                }

            } else if ((string)HttpContext.Application["PRED/PRETH"] == "predstojeci")
            {
                foreach (Aranzman a in listaPocetna)
                {
                    if (a.DatumZavrsetkaPutovanja >= DateTime.Now)
                    {
                        lista.Add(a);
                    }
                }

            }  else
            {
                lista = listaPocetna;
            }

            switch (kriterijum)
            {
                case "Datum pocetka putovanja":
                    lista = lista.OrderBy(i => i.DatumPocetkaPutovanja).ToList();
                    break;

                case "Datum zavrsetka putovanja":
                    lista = lista.OrderBy(i => i.DatumZavrsetkaPutovanja).ToList();
                    break;

                case "Tip prevoza":
                    lista = lista.OrderBy(i => i.TipPrevoza).ToList();
                    break;

                case "Naziv aranzmana":
                    lista = lista.OrderBy(i => i.Naziv).ToList();
                    break;

                case "Cena":
                    lista = lista.OrderBy(i => i.Smestaj.SmestajneJedinice.Min(c => c.Cena)).ToList();
                    break;


            }



            if (smer == "Opadajuce")
            {
                lista.Reverse();
            }

            ViewBag.Aranzmani = lista;

            return View("~/Views/Home/Index.cshtml");
        }

        #endregion


        #region Pretrazi

        public ActionResult Pretrazi(Nullable<DateTime> pocetakOd, Nullable<DateTime> pocetakDo, Nullable<DateTime> zavrsetakOd, Nullable<DateTime> zavrsetakDo, string tipPrevoza, string tipAranzmana, string naziv)
        {
            List<Aranzman> listaPocetna = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            List<Aranzman> lista = new List<Aranzman>();

            

            if ((string)HttpContext.Application["PRED/PRETH"] == "prethodni")
            {
                foreach (Aranzman a in listaPocetna)
                {
                    if (a.DatumZavrsetkaPutovanja <= DateTime.Now)
                    {
                        lista.Add(a);
                    }
                }

            } else if ((string)HttpContext.Application["PRED/PRETH"] == "predstojeci")
            {
                foreach (Aranzman a in listaPocetna)
                {
                    if (a.DatumZavrsetkaPutovanja >= DateTime.Now)
                    {
                        lista.Add(a);
                    }
                }
            }
            else
            {
                lista = listaPocetna;
            }

            if (pocetakOd == null || pocetakDo == null || zavrsetakDo == null || zavrsetakOd == null || tipAranzmana == null || tipPrevoza == null || naziv == null)
            {
                ViewBag.Greska = "Niste uneli sve parametre pretrage!";
                ViewBag.Aranzmani = lista;
                return View("~/Views/Home/Index.cshtml");

            }

            switch (tipPrevoza)
            {
                case "Avion":
                    tipPrevoza = "avion";
                    break;
                case "Autobus":
                    tipPrevoza = "autobus";
                    break;
                case "Autobus + avion":
                    tipPrevoza = "autobusAvion";
                    break;
                case "Individualan":
                    tipPrevoza = "individualan";
                    break;
                case "Ostalo":
                    tipPrevoza = "ostalo";
                    break;
            }

            switch (tipAranzmana)
            {
                case "Nocenje sa doruckom":
                    tipAranzmana = "nocenjeSaDoruckom";
                    break;
                case "Polupansion":
                    tipAranzmana = "poluPansion";
                    break;
                case "Pun pansion":
                    tipAranzmana = "punPansion";
                    break;
                case "All inclusive":
                    tipAranzmana = "allInclusive";
                    break;
                case "Najam apartmana":
                    tipAranzmana = "najamApartmana";
                    break;
            }



            List<Aranzman> listaKonacna = new List<Aranzman>();

            


            foreach (Aranzman a in lista)
            {
                if (a.DatumPocetkaPutovanja >= pocetakOd && a.DatumPocetkaPutovanja <= pocetakDo && a.DatumZavrsetkaPutovanja >= zavrsetakOd && a.DatumZavrsetkaPutovanja <= zavrsetakDo && a.TipAranzmana == tipAranzmana && a.TipPrevoza == tipPrevoza && a.Naziv.Contains(naziv))
                {
                    listaKonacna.Add(a);
                }
            }

            ViewBag.Aranzmani = listaKonacna;
            return View("~/Views/Home/Index.cshtml");
        }

        #endregion


        #region Smestaj

        public ActionResult Smestaj(string naziv)
        {
            List<Aranzman> lista = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            foreach(Aranzman a in lista)
            {
                if(a.Naziv == naziv) {
                    HttpContext.Application["SMESTAJ"] = a.Smestaj;
                    ViewBag.Smestaj = a.Smestaj;
                    ViewBag.SmestajneJedinice = a.Smestaj.SmestajneJedinice;
                    break;
                }
            }
            return View();
        }

        #endregion

        #region Ponisti

        public ActionResult Ponisti() {

            Smestaj s = (Smestaj)HttpContext.Application["SMESTAJ"];

            ViewBag.Smestaj = s;
            ViewBag.SmestajneJedinice = s.SmestajneJedinice;

            return View("~/Views/Home/Smestaj.cshtml");

        }

        #endregion

        #region SmestajPretraga

        public ActionResult SmestajPretraga(string donja, string gornja, string ljubimci, string cenadonja, string cenagornja)
        {
            Smestaj s = (Smestaj)HttpContext.Application["SMESTAJ"];

            if (donja == null || gornja == null || ljubimci == null || cenadonja == null || cenagornja == null )
            {
                ViewBag.SmestajneJedinice = s.SmestajneJedinice;
                ViewBag.Smestaj = s;
                ViewBag.Greska = "Niste uneli sve podatke!";
                return View("~/Views/Home/Smestaj.cshtml");
            }

            int g;
            if (!Int32.TryParse(donja, out g) || !Int32.TryParse(gornja, out g) || !Int32.TryParse(cenadonja, out g) || !Int32.TryParse(cenagornja, out g))
            {
                ViewBag.SmestajneJedinice = s.SmestajneJedinice;
                ViewBag.Smestaj = s;
                ViewBag.Greska = "Niste uneli sve podatke!";
                return View("~/Views/Home/Smestaj.cshtml");
            }

            

            List<SmestajnaJedinica> lista = new List<SmestajnaJedinica>();

            bool ljubimciBool = ((ljubimci == "Da") ? true : false);

            foreach(SmestajnaJedinica sj in s.SmestajneJedinice)
            {
                if(sj.DozvoljenBrGostiju >= Int32.Parse(donja) && sj.DozvoljenBrGostiju <= Int32.Parse(gornja) && sj.Cena >= Int32.Parse(cenadonja) && sj.Cena <= Int32.Parse(cenagornja) && sj.Ljubimci == ljubimciBool)
                {
                    lista.Add(sj);
                }
            }

            ViewBag.SmestajneJedinice = lista;
            ViewBag.Smestaj = s;

            return View("~/Views/Home/Smestaj.cshtml");
        }

        #endregion

        #region SmestajSortiranje


        public ActionResult SmestajSortiranje(string kriterijum, string smer)
        {

            Smestaj s = (Smestaj)HttpContext.Application["SMESTAJ"];
            List<SmestajnaJedinica> lista = s.SmestajneJedinice;

            switch (kriterijum)
            {
                case "Dozvoljen broj gostiju":
                    lista = lista.OrderBy(i => i.DozvoljenBrGostiju).ToList();
                    break;

                case "Cena":
                    lista = lista.OrderBy(i => i.Cena).ToList();
                    break;

            }



            if (smer == "Opadajuce")
            {
                lista.Reverse();
            }

            ViewBag.SmestajneJedinice = lista;
            ViewBag.Smestaj = s;
            return View("~/Views/Home/Smestaj.cshtml");
        }

        #endregion


        #region Registracija

        public ActionResult Registracija()
        {
            return View("~/Views/Registracija/Registracija.cshtml");
        }

        #endregion

        #region Prijava

        public ActionResult Prijava(string korisnickoIme, string lozinka)
        {
            List<Turista> turisti = (List<Turista>)HttpContext.Application["TURISTI"];
            List<Menadzer> menadzeri = (List<Menadzer>)HttpContext.Application["MENADZERI"];
            List<Administrator> administratori = (List<Administrator>)HttpContext.Application["ADMINISTRATORI"];

            ViewBag.Aranzmani = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            bool nasao = false;

            foreach(Turista t in turisti)
            {
                if(t.KorisnickoIme==korisnickoIme && t.Lozinka == lozinka)
                {
                    nasao = true;
                    HttpContext.Session["USER"] = t;
                    ViewBag.Dobrodosli = $"Dobrodosli {korisnickoIme}! Ulogovani ste kao turista";
                    HttpContext.Session["TIPKORISNIKA"] = "Turista";
                    return View("~/Views/Turista/Index.cshtml");
                }
            }

            if (nasao == false)
            {
                foreach (Menadzer t in menadzeri)
                {
                    if (t.KorisnickoIme == korisnickoIme && t.Lozinka == lozinka)
                    {
                        nasao = true;
                        HttpContext.Session["USER"] = t;
                        ViewBag.Dobrodosli = $"Dobrodosli {korisnickoIme}! Ulogovani ste kao menadzer";
                        HttpContext.Session["TIPKORISNIKA"] = "Menadzer";
                        return View("~/Views/Menadzer/MenadzerIndex.cshtml");
                    }
                }
            }

            if (nasao == false)
            {
                foreach (Administrator t in administratori)
                {
                    if (t.KorisnickoIme == korisnickoIme && t.Lozinka == lozinka)
                    {
                        nasao = true;
                        HttpContext.Session["USER"] = t;
                        ViewBag.Dobrodosli = $"Dobrodosli {korisnickoIme}! Ulogovani ste kao admin";
                        HttpContext.Session["TIPKORISNIKA"] = "Admin";
                        return RedirectToAction("Index", "Admin");
                    }
                }
            }

            ViewBag.PrijavaGreska = "Niste dobro uneli podatke!";
            return View("~/Views/Home/Index.cshtml");

        }

        #endregion

        #region PrikaziKomentare

        public ActionResult PrikaziKomentare(string naziv)
        {
            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["KOMENTARI"];

            List<Komentar> lista = new List<Komentar>();

            foreach(Komentar k in komentari)
            {
                if (k.Aranzman == naziv && k.Vidljiv == true)
                {
                    lista.Add(k);
                }
            }

            ViewBag.Komentari = lista;

            return View();
        }

        #endregion

        #region OdjaviSe

        public ActionResult OdjaviSe()
        {
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["ARANZMANI"];
            List<Turista> turisti = (List<Turista>)HttpContext.Application["TURISTI"];
            List<Menadzer> menadzeri = (List<Menadzer>)HttpContext.Application["MENADZERI"];
            List<Administrator> admini = (List<Administrator>)HttpContext.Application["ADMINISTRATORI"];
            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["KOMENTARI"];

            #region Aranzmani
            string filepath = HostingEnvironment.MapPath("~/App_Data/Aranzmani.xml");

            var xmlDoc = XDocument.Load(filepath);
            var Aranzmani = new XElement("Aranzmani");
            

            foreach (Aranzman a in aranzmani)
            {
                var Aranzman = new XElement("Aranzman");

                var naziv = new XElement("naziv", a.Naziv);
                var tipAranzmana = new XElement("tipAranzmana", a.TipAranzmana);
                var tipPrevoza = new XElement("tipPrevoza", a.TipPrevoza);
                var lokacija = new XElement("lokacija", a.Lokacija);
                var datumPocetkaPutovanja = new XElement("datumPocetkaPutovanja", a.DatumPocetkaPutovanja.ToString());
                var datumZavrsetkaPutovanja = new XElement("datumZavrsetkaPutovanja", a.DatumZavrsetkaPutovanja.ToString());

                var mestoNalazenja = new XElement("mestoNalazenja");
                var adresa = new XElement("adresa", a.MestoNalazenja.Adresa);
                var geoDuzina = new XElement("geoDuzina", a.MestoNalazenja.GeoDuzina);
                var geoSirina = new XElement("geoSirina", a.MestoNalazenja.GeoSirina);

                mestoNalazenja.Add(adresa);
                mestoNalazenja.Add(geoDuzina);
                mestoNalazenja.Add(geoSirina);

                string vreme = a.VremeNalazenja.Hours + ":" + a.VremeNalazenja.Minutes;

                var vremeNalazenja = new XElement("vremeNalazenja", vreme);
                var maxBrojPutnika = new XElement("maxBrojPutnika", a.MaxBrojPutnika);
                var opisAranzmana = new XElement("opisAranzmana", a.OpisAranzmana);
                var programPutovanja = new XElement("programPutovanja", a.ProgramPutovanja);
                var posterAranzmana = new XElement("posterAranzmana", a.PosterAranzmana);                
                var obrisan = new XElement("obrisan", (a.Obrisan == true) ? "Da" : "Ne");

                var smestaj = new XElement("smestaj");


                var tipSmestaja = new XElement("tipSmestaja", a.Smestaj.TipSmestaja);
                var nazivS = new XElement("naziv", a.Smestaj.Naziv);
                var brojZvezdica = new XElement("brojZvezdica", a.Smestaj.BrojZvezdica.ToString());
                var postojanjeBazena = new XElement("postojanjeBazena", (a.Smestaj.PostojanjeBazena == true) ? "Da" : "Ne");
                var postojanjeSpaCentra = new XElement("postojanjeSpaCentra", (a.Smestaj.PostojanjeSpaCentra == true) ? "Da" : "Ne");
                var invaliditet = new XElement("invaliditet", (a.Smestaj.Invaliditet == true) ? "Da" : "Ne");
                var wifi = new XElement("wifi", (a.Smestaj.Wifi == true) ? "Da" : "Ne");
                var obrisanS = new XElement("obrisan", (a.Smestaj.Obrisan == true) ? "Da" : "Ne");

                smestaj.Add(tipSmestaja);
                smestaj.Add(nazivS);
                smestaj.Add(brojZvezdica);
                smestaj.Add(postojanjeBazena);
                smestaj.Add(postojanjeSpaCentra);
                smestaj.Add(invaliditet);
                smestaj.Add(wifi);
                smestaj.Add(obrisanS);

                var smestajneJedinice = new XElement("smestajneJedinice");


                foreach (SmestajnaJedinica sj in a.Smestaj.SmestajneJedinice)
                {
                    var jedinica = new XElement("jedinica");

                    var brojSmestajneJedinice = new XElement("brojSmestajneJedinice", sj.BrojSmestajneJedinice.ToString());
                    var dozvoljenBrGostiju = new XElement("dozvoljenBrGostiju", sj.DozvoljenBrGostiju.ToString());
                    var ljubimci = new XElement("ljubimci", (sj.Ljubimci == true) ? "Da" : "Ne");
                    var cena = new XElement("cena", sj.Cena.ToString());
                    var zauzeta = new XElement("zauzeta", (sj.Zauzeta == true) ? "Da" : "Ne");
                    var obrisanaJ = new XElement("obrisan", (sj.Obrisana == true) ? "Da" : "Ne");

                    jedinica.Add(brojSmestajneJedinice);
                    jedinica.Add(dozvoljenBrGostiju);
                    jedinica.Add(ljubimci);
                    jedinica.Add(cena);
                    jedinica.Add(zauzeta);
                    jedinica.Add(obrisanaJ);

                    smestajneJedinice.Add(jedinica);
                }

                smestaj.Add(smestajneJedinice);


                Aranzman.Add(naziv);
                Aranzman.Add(tipAranzmana);
                Aranzman.Add(tipPrevoza);
                Aranzman.Add(lokacija);
                Aranzman.Add(datumPocetkaPutovanja);
                Aranzman.Add(datumZavrsetkaPutovanja);
                Aranzman.Add(mestoNalazenja);
                Aranzman.Add(vremeNalazenja);
                Aranzman.Add(maxBrojPutnika);
                Aranzman.Add(opisAranzmana);
                Aranzman.Add(programPutovanja);
                Aranzman.Add(posterAranzmana);
                Aranzman.Add(obrisan);
                Aranzman.Add(smestaj);

                Aranzmani.Add(Aranzman);
            }


            XDocument document = new XDocument(new XDeclaration("0.1", "utf-8", "yes"), Aranzmani);

            document.Save(filepath);
            #endregion

            #region Turisti

            filepath = HostingEnvironment.MapPath("~/App_Data/Turisti.xml");

            xmlDoc = XDocument.Load(filepath);
            var Turisti = new XElement("Turisti");

            foreach (Turista t in turisti)
            {
                var Turista = new XElement("Turista");

                var korisnickoIme = new XElement("korisnickoIme", t.KorisnickoIme);
                var lozinka = new XElement("lozinka", t.Lozinka);
                var ime = new XElement("ime", t.Ime);
                var prezime = new XElement("prezime", t.Prezime);
                var pol = new XElement("pol", t.Pol);
                var email = new XElement("email", t.Email);
                var datumRodjenja = new XElement("datumRodjenja", t.DatumRodjenja.ToString());
                var obrisan = new XElement("obrisan", (t.Obrisan == true) ? "Da" : "Ne");

                var rezervacije = new XElement("rezervacije");

                foreach(Rezervacija r in t.Rezervacije)
                {
                    var rezervacija = new XElement("rezervacija");

                    var jir = new XElement("jir", r.Identifikator);
                    var imeTuriste = new XElement("imeTuriste", r.TuristaKojiRezervise);
                    var status = new XElement("status", (r.Status == true) ? "Aktivna" : "Otkazana");
                    var aranzman = new XElement("aranzman", r.Aranzman);
                    var smestajnaJedinica = new XElement("smestajnaJedinica", r.SmestajnaJedinica.BrojSmestajneJedinice);

                    rezervacija.Add(jir);
                    rezervacija.Add(imeTuriste);
                    rezervacija.Add(status);
                    rezervacija.Add(aranzman);
                    rezervacija.Add(smestajnaJedinica);

                    rezervacije.Add(rezervacija);
                }

                Turista.Add(korisnickoIme);
                Turista.Add(lozinka);
                Turista.Add(ime);
                Turista.Add(prezime);
                Turista.Add(pol);
                Turista.Add(email);
                Turista.Add(datumRodjenja);
                Turista.Add(obrisan);
                Turista.Add(rezervacije);

                Turisti.Add(Turista);
            }

            document = new XDocument(new XDeclaration("0.1", "utf-8", "yes"), Turisti);

            document.Save(filepath);

            #endregion

            #region Menadzeri

            filepath = HostingEnvironment.MapPath("~/App_Data/Menadzeri.xml");

            xmlDoc = XDocument.Load(filepath);

            var Menadzeri = new XElement("Menadzeri");

            foreach (Menadzer t in menadzeri)
            {
                var Menadzer = new XElement("Menadzer");

                var korisnickoIme = new XElement("korisnickoIme", t.KorisnickoIme);
                var lozinka = new XElement("lozinka", t.Lozinka);
                var ime = new XElement("ime", t.Ime);
                var prezime = new XElement("prezime", t.Prezime);
                var pol = new XElement("pol", t.Pol);
                var email = new XElement("email", t.Email);
                var datumRodjenja = new XElement("datumRodjenja", t.DatumRodjenja.ToString());

                var aranzmaniX = new XElement("aranzmani");

                foreach(Aranzman a in t.Aranzmani)
                {
                    var aranzman = new XElement("aranzman", a.Naziv);

                    aranzmaniX.Add(aranzman);
                    
                }

                Menadzer.Add(korisnickoIme);
                Menadzer.Add(lozinka);
                Menadzer.Add(ime);
                Menadzer.Add(prezime);
                Menadzer.Add(pol);
                Menadzer.Add(email);
                Menadzer.Add(datumRodjenja);
                Menadzer.Add(aranzmaniX);


                Menadzeri.Add(Menadzer);

            }

            document = new XDocument(new XDeclaration("0.1", "utf-8", "yes"), Menadzeri);

            document.Save(filepath);

            #endregion

            #region Komentar

            filepath = HostingEnvironment.MapPath("~/App_Data/Komentari.xml");

            xmlDoc = XDocument.Load(filepath);

            var Komentari = new XElement("Komentari");

            foreach (Komentar k in komentari)
            {
                var Komentar = new XElement("Komentar");

                var turista = new XElement("turista", k.Turista);
                var aranzman = new XElement("aranzman", k.Aranzman);
                var tekst = new XElement("tekst", k.Tekst);
                var ocena = new XElement("ocena", k.Ocena);
                var vidljiv = new XElement("vidljiv", (k.Vidljiv == true) ? "Da" : "Ne");

                Komentar.Add(turista);
                Komentar.Add(aranzman);
                Komentar.Add(tekst);
                Komentar.Add(ocena);
                Komentar.Add(vidljiv);

                Komentari.Add(Komentar);
            }

            document = new XDocument(new XDeclaration("0.1", "utf-8", "yes"), Komentari);

            document.Save(filepath);

            #endregion

            ViewBag.Aranzmani = (List<Aranzman>)HttpContext.Application["ARANZMANI"];
            return View("~/Views/Home/Index.cshtml");
        }

        #endregion

    }
}