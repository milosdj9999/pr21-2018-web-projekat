﻿using PR21_2018_Web_Projekat.Models;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR21_2018_Web_Projekat.Controllers
{
    public class AdminController : Controller
    {

        #region Index
        // GET: Admin
        public ActionResult Index()
        {
            List<Turista> lista = new List<Turista>();

            List<Turista> turisti = (List<Turista>)HttpContext.Application["TURISTI"];
            List<Menadzer> menadzeri = (List<Menadzer>)HttpContext.Application["MENADZERI"];
            List<Administrator> admini = (List<Administrator>)HttpContext.Application["ADMINISTRATORI"];

            foreach (Turista t in turisti)
            {
                lista.Add(t);
            }

           
            foreach (Menadzer m in menadzeri)
            {
                Turista temp = new Turista(m.KorisnickoIme, m.Lozinka, m.Ime, m.Prezime, m.Pol, m.Email, m.DatumRodjenja, m.Uloga, new List<Rezervacija>(), false);

                
                lista.Add(temp);
            }

            foreach (Administrator m in admini)
            {
                Turista temp = new Turista(m.KorisnickoIme, m.Lozinka, m.Ime, m.Prezime, m.Pol, m.Email, m.DatumRodjenja, m.Uloga, new List<Rezervacija>(), false);

            
                lista.Add(temp);
            }

            ViewBag.Svi = lista;
            HttpContext.Session["SVI"] = lista;
            return View("~/Views/Admin/AdminIndex.cshtml");
        }

        #endregion

        #region DodajMenadzera

        public ActionResult DodajMenadzera(string korisnickoIme, string lozinka, string ime, string prezime, string pol, string email, Nullable<DateTime> datum)
        {
            if(korisnickoIme==null || lozinka == null || ime == null || prezime == null || pol == null || email == null || datum == null)
            {
                ViewBag.Poruka = "Niste uneli sve podatke";
                return View("~/Views/Admin/DodajMenadzera.cshtml");
            }

            DateTime datumRodjenja = (DateTime)datum;
            List<Aranzman> lista = new List<Aranzman>();

            Menadzer m = new Menadzer(korisnickoIme, lozinka, ime, prezime, pol, email, datumRodjenja, EUloga.menadzer, lista);

            List<Menadzer> menadzeri = (List<Menadzer>)HttpContext.Application["MENADZERI"];

            menadzeri.Add(m);

            HttpContext.Application["MENADZERI"] = menadzeri;


            return RedirectToAction("Index", "Admin");
        }

        #endregion

        #region SumnjiviTuristi

        public ActionResult SumnjiviTuristi()
        {
            List<Turista> turisti = (List<Turista>)HttpContext.Application["TURISTI"];

            List<Turista> lista = new List<Turista>();

            foreach(Turista t in turisti)
            {
                int otkazao = 0;

                foreach (Rezervacija r in t.Rezervacije)
                {
                    if(r.Status == false)
                    {
                        otkazao++;
                    }
                }

                if(otkazao >= 2)
                {
                    lista.Add(t);

                }
            }

            ViewBag.Turisti = lista;
            return View();
        }

        #endregion

        #region BlokirajTuristu

        public ActionResult BlokirajTuristu(string naziv)
        {
            List<Turista> turisti = (List<Turista>)HttpContext.Application["TURISTI"];

            foreach(Turista t in turisti)
            {
                if(t.KorisnickoIme == naziv)
                {
                    t.Obrisan = true;
                    break;
                }
            }
            return View("~/Views/Admin/SumnjiviTuristi.cshtml");
        }

        #endregion

        #region Sortiraj

        public ActionResult Sortiraj(string kriterijum, string smer)
        {

            List<Turista> lista = (List<Turista>)HttpContext.Session["SVI"];

            switch (kriterijum)
            {
                case "Ime":
                    lista = lista.OrderBy(i => i.Ime).ToList();
                    break;

                case "Prezime":
                    lista = lista.OrderBy(i => i.Prezime).ToList();
                    break;

                case "Uloga":
                    lista = lista.OrderBy(i => i.Uloga.ToString()).ToList();
                    break;
            }



            if (smer == "Opadajuce")
            {
                lista.Reverse();
            }

            ViewBag.Svi = lista;

            return View("~/Views/Admin/AdminIndex.cshtml");
        }

        #endregion

        #region Pretrazi

        public ActionResult Pretrazi(string ime, string prezime, string uloga)
        {
            List<Turista> lista = (List<Turista>)HttpContext.Session["SVI"];

            if (ime == null || prezime == null || uloga == null)
            {
                ViewBag.Greska = "Niste uneli sve parametre pretrage!";
                ViewBag.Svi = lista;
                return View("~/Views/Admin/AdminIndex.cshtml");

            }

            EUloga euloga = new EUloga();

            switch (uloga)
            {
                case "Turista":
                    euloga = EUloga.turista;
                    break;
                case "Menadzer":
                    euloga = EUloga.menadzer;
                    break;
                case "Administrator":
                    euloga = EUloga.administrator;
                    break;
                
            }


            List<Turista> listaKonacna = new List<Turista>();


            foreach (Turista t in lista)
            {
                if (t.Ime.Contains(ime) && t.Prezime.Contains(prezime) && t.Uloga == euloga)
                {
                    listaKonacna.Add(t);
                }
            }

            ViewBag.Svi = listaKonacna;
            return View("~/Views/Admin/AdminIndex.cshtml");
        }

        #endregion
    }
}