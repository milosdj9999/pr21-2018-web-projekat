﻿using PR21_2018_Web_Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR21_2018_Web_Projekat.Controllers
{
    public class TuristaController : Controller
    {
        // GET: Turista

        #region Index
        public ActionResult Index()
        {
            HttpContext.Application["PRED/PRETH"] = "predstojeci";

            List<Aranzman> lista = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            lista = lista.OrderBy(i => i.DatumZavrsetkaPutovanja).ToList();

            lista.Reverse();

            ViewBag.Aranzmani = lista;

            return View();
        }

        #endregion


        #region Prethodni

        public ActionResult Prethodni()
        {
            HttpContext.Application["PRED/PRETH"] = "prethodni";
            List<Aranzman> lista = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            List<Aranzman> lista2 = new List<Aranzman>();

            foreach (Aranzman a in lista)
            {
                if (a.DatumPocetkaPutovanja <= DateTime.Now)
                {
                    lista2.Add(a);
                }
            }

            lista2 = lista2.OrderBy(i => i.DatumZavrsetkaPutovanja).ToList();
            lista2.Reverse();
            ViewBag.Aranzmani = lista2;

            return View("~/Views/Turista/Index.cshtml");
        }

        #endregion


        #region Predstojeci

        public ActionResult Predstojeci()
        {
            HttpContext.Application["PRED/PRETH"] = "predstojeci";
            List<Aranzman> lista = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            List<Aranzman> lista2 = new List<Aranzman>();

            foreach (Aranzman a in lista)
            {
                if (a.DatumPocetkaPutovanja >= DateTime.Now)
                {
                    lista2.Add(a);
                }
            }

            lista2 = lista2.OrderBy(i => i.DatumZavrsetkaPutovanja).ToList();
            lista2.Reverse();
            ViewBag.Aranzmani = lista2;

            return View("~/Views/Turista/Index.cshtml");
        }

        #endregion


        #region Pretrazi

        public ActionResult Pretrazi(Nullable<DateTime> pocetakOd, Nullable<DateTime> pocetakDo, Nullable<DateTime> zavrsetakOd, Nullable<DateTime> zavrsetakDo, string tipPrevoza, string tipAranzmana, string naziv)
        {
            List<Aranzman> listaPocetna = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            List<Aranzman> lista = new List<Aranzman>();



            if ((string)HttpContext.Application["PRED/PRETH"] == "prethodni")
            {
                foreach (Aranzman a in listaPocetna)
                {
                    if (a.DatumZavrsetkaPutovanja <= DateTime.Now)
                    {
                        lista.Add(a);
                    }
                }

            }
            else if ((string)HttpContext.Application["PRED/PRETH"] == "predstojeci")
            {
                foreach (Aranzman a in listaPocetna)
                {
                    if (a.DatumZavrsetkaPutovanja >= DateTime.Now)
                    {
                        lista.Add(a);
                    }
                }
            } else
            {
                lista = listaPocetna;
            }

            if (pocetakOd == null || pocetakDo == null || zavrsetakDo == null || zavrsetakOd == null || tipAranzmana == null || tipPrevoza == null || naziv == null)
            {
                ViewBag.Greska = "Niste uneli sve parametre pretrage!";
                ViewBag.Aranzmani = lista;
                return View("~/Views/Turista/Index.cshtml");

            }

            switch (tipPrevoza)
            {
                case "Avion":
                    tipPrevoza = "avion";
                    break;
                case "Autobus":
                    tipPrevoza = "autobus";
                    break;
                case "Autobus + avion":
                    tipPrevoza = "autobusAvion";
                    break;
                case "Individualan":
                    tipPrevoza = "individualan";
                    break;
                case "Ostalo":
                    tipPrevoza = "ostalo";
                    break;
            }

            switch (tipAranzmana)
            {
                case "Nocenje sa doruckom":
                    tipAranzmana = "nocenjeSaDoruckom";
                    break;
                case "Polupansion":
                    tipAranzmana = "poluPansion";
                    break;
                case "Pun pansion":
                    tipAranzmana = "punPansion";
                    break;
                case "All inclusive":
                    tipAranzmana = "allInclusive";
                    break;
                case "Najam apartmana":
                    tipAranzmana = "najamApartmana";
                    break;
            }



            List<Aranzman> listaKonacna = new List<Aranzman>();




            foreach (Aranzman a in lista)
            {
                if (a.DatumPocetkaPutovanja >= pocetakOd && a.DatumPocetkaPutovanja <= pocetakDo && a.DatumZavrsetkaPutovanja >= zavrsetakOd && a.DatumZavrsetkaPutovanja <= zavrsetakDo && a.TipAranzmana == tipAranzmana && a.TipPrevoza == tipPrevoza && a.Naziv.Contains(naziv))
                {
                    listaKonacna.Add(a);
                }
            }

            ViewBag.Aranzmani = listaKonacna;
            return View("~/Views/Turista/Index.cshtml");
        }

        #endregion


        #region Sortiraj

        public ActionResult Sortiraj(string kriterijum, string smer)
        {
            List<Aranzman> listaPocetna = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            List<Aranzman> lista = new List<Aranzman>();

            if ((string)HttpContext.Application["PRED/PRETH"] == "prethodni")
            {
                foreach (Aranzman a in listaPocetna)
                {
                    if (a.DatumZavrsetkaPutovanja < DateTime.Now)
                    {
                        lista.Add(a);
                    }
                }

            }
            else if ((string)HttpContext.Application["PRED/PRETH"] == "prethodni")
            {
                foreach (Aranzman a in listaPocetna)
                {
                    if (a.DatumZavrsetkaPutovanja > DateTime.Now)
                    {
                        lista.Add(a);
                    }
                }
            } else
            {
                lista = listaPocetna;
            }

            switch (kriterijum)
            {
                case "Datum pocetka putovanja":
                    lista = lista.OrderBy(i => i.DatumPocetkaPutovanja).ToList();
                    break;

                case "Datum zavrsetka putovanja":
                    lista = lista.OrderBy(i => i.DatumZavrsetkaPutovanja).ToList();
                    break;

                case "Tip prevoza":
                    lista = lista.OrderBy(i => i.TipPrevoza).ToList();
                    break;

                case "Naziv aranzmana":
                    lista = lista.OrderBy(i => i.Naziv).ToList();
                    break;

                case "Cena":
                    lista = lista.OrderBy(i => i.Smestaj.SmestajneJedinice.Min(c => c.Cena)).ToList();
                    break;


            }



            if (smer == "Opadajuce")
            {
                lista.Reverse();
            }

            ViewBag.Aranzmani = lista;

            return View("~/Views/Turista/Index.cshtml");
        }

        #endregion

        #region Smestaj

        public ActionResult Smestaj(string naziv)
        {
            List<Aranzman> lista = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            foreach (Aranzman a in lista)
            {
                if (a.Naziv == naziv)
                {
                    HttpContext.Application["SMESTAJ"] = a.Smestaj;
                    ViewBag.Smestaj = a.Smestaj;
                    ViewBag.SmestajneJedinice = a.Smestaj.SmestajneJedinice;
                    break;
                }
            }
            return View("~/Views/Turista/TuristaSmestaj.cshtml");
        }

        #endregion

        #region PonistiPretraguSmestaj

        public ActionResult PonistiPretraguSmestaj()
        {
            Smestaj s = (Smestaj)HttpContext.Application["SMESTAJ"];

            ViewBag.Smestaj = s;
            ViewBag.SmestajneJedinice = s.SmestajneJedinice;

            return View("~/Views/Turista/TuristaSmestaj.cshtml");
        }

        #endregion

        #region SmestajPretraga

        public ActionResult SmestajPretraga(string donja, string gornja, string ljubimci, string cenadonja, string cenagornja)
        {

            if (donja == null || gornja == null || ljubimci == null || cenadonja == null || cenagornja == null)
            {
                ViewBag.Smestaj = (Smestaj)HttpContext.Application["SMESTAJ"];
                ViewBag.SmestajneJedinice = ((Smestaj)HttpContext.Application["SMESTAJ"]).SmestajneJedinice;
                ViewBag.Greska = "Niste dobro uneli podatke!";
                return View("~/Views/Turista/TuristaSmestaj.cshtml");
            }

            int g;
            if (!Int32.TryParse(donja, out g) || !Int32.TryParse(gornja, out g) || !Int32.TryParse(cenadonja, out g) || !Int32.TryParse(cenagornja, out g))
            {
                ViewBag.Smestaj = (Smestaj)HttpContext.Application["SMESTAJ"];
                ViewBag.SmestajneJedinice = ((Smestaj)HttpContext.Application["SMESTAJ"]).SmestajneJedinice;
                ViewBag.Greska = "Niste dobro uneli podatke!";
                return View("~/Views/Turista/TuristaSmestaj.cshtml");
            }

            Smestaj s = (Smestaj)HttpContext.Application["SMESTAJ"];

            List<SmestajnaJedinica> lista = new List<SmestajnaJedinica>();

            bool ljubimciBool = ((ljubimci == "Da") ? true : false);

            foreach (SmestajnaJedinica sj in s.SmestajneJedinice)
            {
                if (sj.DozvoljenBrGostiju >= Int32.Parse(donja) && sj.DozvoljenBrGostiju <= Int32.Parse(gornja) && sj.Cena >= Int32.Parse(cenadonja) && sj.Cena <= Int32.Parse(cenagornja) && sj.Ljubimci == ljubimciBool)
                {
                    lista.Add(sj);
                }
            }

            ViewBag.SmestajneJedinice = lista;
            ViewBag.Smestaj = s;

            return View("~/Views/Turista/TuristaSmestaj.cshtml");
        }

        #endregion

        #region SmestajSortiranje


        public ActionResult SmestajSortiranje(string kriterijum, string smer)
        {

            Smestaj s = (Smestaj)HttpContext.Application["SMESTAJ"];
            List<SmestajnaJedinica> lista = s.SmestajneJedinice;

            switch (kriterijum)
            {
                case "Dozvoljen broj gostiju":
                    lista = lista.OrderBy(i => i.DozvoljenBrGostiju).ToList();
                    break;

                case "Cena":
                    lista = lista.OrderBy(i => i.Cena).ToList();
                    break;

            }



            if (smer == "Opadajuce")
            {
                lista.Reverse();
            }

            ViewBag.SmestajneJedinice = lista;
            ViewBag.Smestaj = s;
            return View("~/Views/Turista/TuristaSmestaj.cshtml");
        }

        #endregion

        #region MojeRezervacije

        public ActionResult MojeRezervacije()
        {
            Turista t = (Turista)HttpContext.Session["USER"];
            ViewBag.Rezervacije = t.Rezervacije;
            return View("~/Views/Turista/Rezervacije.cshtml");
        }
        #endregion

        #region Rezervacija

        public ActionResult Rezervacija(string prikazi)
        {

            List<Aranzman> lista = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            foreach (Aranzman a in lista)
            {
                if (a.Naziv == prikazi)
                {
                    HttpContext.Session["ARANZMAN"] = a;
                    ViewBag.Aranzman = a;
                    break;
                }
            }

            return View("~/Views/Turista/TuristaRezervisi.cshtml");


        }

        #endregion

        #region NazadNaSmestaj

        public ActionResult NazadNaSmestaj()
        {
            Aranzman a = (Aranzman)HttpContext.Session["ARANZMAN"];

            if (a != null)
            {
                ViewBag.Aranzman = a;

                return View("~/Views/Turista/TuristaRezervisi.cshtml");

            } else
            {
                return View("~/Views/Turista/Rezervacije.cshtml");
            }


        }
        #endregion


        #region Rezervisi

        public ActionResult Rezervisi(string aranzman, string broj)
        {
            int br = Int32.Parse(broj);

            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["ARANZMANI"];

            Aranzman ar = null;
            SmestajnaJedinica sjj = null;
            foreach (Aranzman a in aranzmani)
            {
                if (a.naziv == aranzman)
                {
                    foreach (SmestajnaJedinica sj in a.Smestaj.SmestajneJedinice)
                    {
                        if (sj.BrojSmestajneJedinice == br)
                        {
                            sj.Zauzeta = true;
                            ar = a;
                            sjj = sj;

                            break;
                        }
                    }
                    break;
                }

            }

            HttpContext.Application["ARANZMANI"] = aranzmani;

            string jir = RandomString(15);

            Turista t = (Turista)HttpContext.Session["USER"];

            Rezervacija rez = new Rezervacija(jir, t.KorisnickoIme, true, ar.Naziv, sjj);



            List<Turista> turisti = (List<Turista>)HttpContext.Application["TURISTI"];


            foreach (Turista tr in turisti)
            {
                if (tr.KorisnickoIme == t.KorisnickoIme)
                {
                    tr.Rezervacije.Add(rez);
                    break;
                }
            }


            HttpContext.Application["TURISTI"] = turisti;

            ViewBag.Rezervacije = t.Rezervacije;




            return View("~/Views/Turista/Rezervacije.cshtml");
        }

        #endregion


        #region Otkazi

        public ActionResult Otkazi(string rez, string aranzman, string br)
        {
            Turista t = (Turista)HttpContext.Session["USER"];

            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["ARANZMANI"];


            foreach (Aranzman a in aranzmani)
            {
                if (a.naziv == aranzman)
                {
                    if (a.DatumPocetkaPutovanja <= DateTime.Now)
                    {
                        ViewBag.Poruka = "Navedena rezervacija je prosla i nemoguce ju je otkazati!";
                        ViewBag.Rezervacije = t.Rezervacije;
                        return View("~/Views/Turista/Rezervacije.cshtml");
                    }

                    foreach (SmestajnaJedinica sj in a.Smestaj.SmestajneJedinice)
                    {
                        if (sj.BrojSmestajneJedinice == Int32.Parse(br))
                        {
                            sj.Zauzeta = false;

                        }
                    }
                }

            }

            foreach (Rezervacija r in t.Rezervacije)
            {
                if (r.Identifikator == rez)
                {
                    r.Status = false;
                }
            }

            ViewBag.Rezervacije = t.Rezervacije;

            ViewBag.Poruka = $"Uspesno ste otkazali rezervaciju {aranzman}, smestajna jedinica broj {br}";
            return View("~/Views/Turista/Rezervacije.cshtml");
        }

        #endregion

        #region RandomString

        public static string RandomString(int length)
        {
            Random rand = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[rand.Next(s.Length)]).ToArray());
        }

        #endregion

        #region Pregled

        public ActionResult Pregled(string kriterijum)
        {
            Turista t = (Turista)HttpContext.Session["USER"];

            List<Rezervacija> rezervacije = t.Rezervacije;

            List<Rezervacija> lista = new List<Rezervacija>();


            foreach (Rezervacija r in rezervacije)
            {
                switch (kriterijum)
                {
                    case "Aktivne":
                        if (r.Status == true)
                        {
                            lista.Add(r);
                        }
                        break;

                    case "Otkazane":
                        if (r.Status == false)
                        {
                            lista.Add(r);
                        }
                        break;
                    case "Prethodne":
                        if (nadjiAranzmanPoImenu(r.Aranzman).DatumPocetkaPutovanja <= DateTime.Now)
                        {
                            lista.Add(r);
                        }
                        break;

                    case "Buduce":
                        if (nadjiAranzmanPoImenu(r.Aranzman).DatumPocetkaPutovanja >= DateTime.Now)
                        {
                            lista.Add(r);
                        }
                        break;

                }
            }

            ViewBag.Rezervacije = lista;
            return View("~/Views/Turista/Rezervacije.cshtml");
        }

        #endregion

        #region nadjiAranzmanPoImenu

        private Aranzman nadjiAranzmanPoImenu(string ime)
        {
            List<Aranzman> aranzmani = (List<Aranzman>)HttpContext.Application["ARANZMANI"];


            foreach (Aranzman a in aranzmani)
            {
                if (a.Naziv == ime)
                {
                    return a;
                }
            }

            return null;
        }

        #endregion

        #region Komentar

        public ActionResult Komentar(string aranzman)
        {
            Aranzman a = nadjiAranzmanPoImenu(aranzman);

            if (a.DatumZavrsetkaPutovanja <= DateTime.Now)
            {
                ViewBag.Poruka = "Ne mozete ostaviti komentar na aranzmanu koji jos nije prosao!";
                Turista t = (Turista)HttpContext.Session["USER"];
                ViewBag.Rezervacije = t.Rezervacije;
                return View("~/Views/Turista/Rezervacije.cshtml");
            }

            HttpContext.Session["Aranzman"] = a;




            return View("~/Views/Turista/Komentar.cshtml");
        }

        #endregion

        #region PotvrdiKomentar

        public ActionResult PotvrdiKomentar(string kom, string zvezda)
        {
            Turista t = (Turista)HttpContext.Session["USER"];
            Aranzman a = (Aranzman)HttpContext.Session["Aranzman"];
            Komentar k = new Komentar(t.KorisnickoIme, a.Naziv, kom, Int32.Parse(zvezda), false);

            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["KOMENTARI"];
            komentari.Add(k);
            HttpContext.Application["KOMENTARI"] = komentari;

            ViewBag.Poruka = "Uspesno ste dodali novi komentar!";

            ViewBag.Rezervacije = (List<Rezervacija>)((Turista)HttpContext.Session["USER"]).Rezervacije;

            return View("~/Views/Turista/Rezervacije.cshtml");

        }

        #endregion

        #region RezervacijePretraga

        public ActionResult RezervacijePretraga(string id, string status, string aranzman)
        {

            if (id == null || status == null || aranzman == null)
            {
                ViewBag.Greska = "Niste dobro uneli podatke!";
                return View("~/Views/Turista/Rezervacije.cshtml");
            }



            Turista t = (Turista)HttpContext.Session["USER"];

            List<Rezervacija> lista = t.Rezervacije;

            List<Rezervacija> lista2 = new List<Rezervacija>();
            bool statusBool = ((status == "Aktivna") ? true : false);

            foreach (Rezervacija r in lista)

                if (r.Identifikator.Contains(id) && r.Status == statusBool && r.Aranzman.Contains(aranzman))
                {
                    lista2.Add(r);
                }


            ViewBag.Rezervacije = lista2;

            return View("~/Views/Turista/Rezervacije.cshtml");
        }

        #endregion

        #region RezervacijeSortiranje

        public ActionResult RezervacijeSortiranje(string kriterijum, string smer)
        {

            Turista t = (Turista)HttpContext.Session["USER"];

            List<Rezervacija> lista = t.Rezervacije;

            switch (kriterijum)
            {
                case "Identifikator":
                    lista = lista.OrderBy(i => i.Identifikator).ToList();
                    break;

                case "Aranzman":
                    lista = lista.OrderBy(i => i.Aranzman).ToList();
                    break;

            }



            if (smer == "Opadajuce")
            {
                lista.Reverse();
            }

            ViewBag.Rezervacije = lista;
            return View("~/Views/Turista/Rezervacije.cshtml");
        }

        #endregion

        #region PrikaziKomentare

        public ActionResult PrikaziKomentare(string naziv)
        {
            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["KOMENTARI"];

            List<Komentar> lista = new List<Komentar>();

            foreach (Komentar k in komentari)
            {
                if (k.Aranzman == naziv && k.Vidljiv == true)
                {
                    lista.Add(k);
                }
            }

            ViewBag.Komentari = lista;

            return View("~/Views/Home/PrikaziKomentare.cshtml");
        }

        #endregion

        #region UrediProfil

        public ActionResult UrediProfil()
        {
            switch ((string)(HttpContext.Session["TIPKORISNIKA"]))
            {
                case "Turista":
                    Turista t = (Turista)HttpContext.Session["USER"];

                    ViewBag.Korisnik = t;
                    break;
                case "Menadzer":
                    Menadzer m = (Menadzer)HttpContext.Session["USER"];

                    ViewBag.Korisnik = m;
                    break;

                case "Admin":
                    Administrator a = (Administrator)HttpContext.Session["USER"];

                    ViewBag.Korisnik = a;
                    break;

            }

            ViewBag.Tip = (string)(HttpContext.Session["TIPKORISNIKA"]);

            if ((string)HttpContext.Session["GRESKA"] == "Da")
            {
                ViewBag.Greska = "Niste dobro uneli podatke!";
            }
            return View();
        }

        #endregion


        #region PotvrdiProfil

        public ActionResult PotvrdiProfil(string staroKorisnickoIme, string korisnickoIme, string lozinka, string ime, string prezime, string pol, string email, Nullable<DateTime> datum)
        {
            if (korisnickoIme == null || lozinka == null || ime == null || prezime == null || pol == null || email == null || datum == null)
            {
                ViewBag.Greska = "Niste dobrp uneli sve podatke!";
                HttpContext.Session["GRESKA"] = "Da";
                return RedirectToAction("UrediProfil", "Turista");
            }

            DateTime datumm = (DateTime)datum;

            switch ((string)(HttpContext.Session["TIPKORISNIKA"]))
            {
                case "Turista":

                    Turista t = (Turista)HttpContext.Session["USER"];

                    List<Turista> turisti = (List<Turista>)HttpContext.Application["TURISTI"];

                    foreach(Turista tr in turisti)
                    {
                        if(tr.KorisnickoIme == staroKorisnickoIme)
                        {
                            tr.KorisnickoIme = korisnickoIme;
                            tr.Lozinka = lozinka;
                            tr.Ime = ime;
                            tr.Prezime = prezime;
                            tr.Pol = pol;
                            tr.Email = email;
                            tr.DatumRodjenja = datumm;

                        }
                    }

                    HttpContext.Application["TURISTI"] = turisti;

                    t.KorisnickoIme = korisnickoIme;
                    t.Lozinka = lozinka;
                    t.Ime = ime;
                    t.Prezime = prezime;
                    t.Pol = pol;
                    t.Email = email;
                    t.DatumRodjenja = datumm;

                    HttpContext.Session["USER"] = t;

                    return RedirectToAction("Index", "Turista");
                    break;

                case "Menadzer":
                    Menadzer m = (Menadzer)HttpContext.Session["USER"];

                    List<Menadzer> menadzeri = (List<Menadzer>)HttpContext.Application["MENADZERI"];

                    foreach (Menadzer tr in menadzeri)
                    {
                        if (tr.KorisnickoIme == staroKorisnickoIme)
                        {
                            tr.KorisnickoIme = korisnickoIme;
                            tr.Lozinka = lozinka;
                            tr.Ime = ime;
                            tr.Prezime = prezime;
                            tr.Pol = pol;
                            tr.Email = email;
                            tr.DatumRodjenja = datumm;

                        }
                    }

                    HttpContext.Application["MENADZERI"] = menadzeri;

                    m.KorisnickoIme = korisnickoIme;
                    m.Lozinka = lozinka;
                    m.Ime = ime;
                    m.Prezime = prezime;
                    m.Pol = pol;
                    m.Email = email;
                    m.DatumRodjenja = datumm;

                    HttpContext.Session["USER"] = m;

                    return RedirectToAction("Index", "Menadzer");
                    break;

                case "Admin":
                    Administrator a = (Administrator)HttpContext.Session["USER"];

                    List<Administrator> admini = (List<Administrator>)HttpContext.Application["ADMINISTRATORI"];

                    foreach (Administrator tr in admini)
                    {
                        if (tr.KorisnickoIme == staroKorisnickoIme)
                        {
                            tr.KorisnickoIme = korisnickoIme;
                            tr.Lozinka = lozinka;
                            tr.Ime = ime;
                            tr.Prezime = prezime;
                            tr.Pol = pol;
                            tr.Email = email;
                            tr.DatumRodjenja = datumm;

                        }
                    }

                    HttpContext.Application["ADMINISTRATORI"] = admini;

                    a.KorisnickoIme = korisnickoIme;
                    a.Lozinka = lozinka;
                    a.Ime = ime;
                    a.Prezime = prezime;
                    a.Pol = pol;
                    a.Email = email;
                    a.DatumRodjenja = datumm;

                    HttpContext.Session["USER"] = a;

                    
                    return RedirectToAction("Index", "Admin");

                    break;

                default:

                    return RedirectToAction("Index", "Home");
            }

            
        }


        #endregion
    }
}