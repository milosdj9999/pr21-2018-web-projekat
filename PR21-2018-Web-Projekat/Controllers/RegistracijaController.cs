﻿using PR21_2018_Web_Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR21_2018_Web_Projekat.Controllers
{
    public class RegistracijaController : Controller
    {
        // GET: Registracija
        public ActionResult Index(string korisnickoIme, string lozinka, string ime, string prezime, string pol, string email, Nullable<DateTime> datum)
        {
            if(korisnickoIme==null || lozinka==null || ime==null || prezime==null || pol==null || email==null || datum == null)
            {
                ViewBag.Greska = "Niste uneli sve podatke!";
                RedirectToAction("/Registracija/Registracija");
            }

            List<Turista> turisti = (List<Turista>)HttpContext.Application["TURISTI"];
            List<Menadzer> menadzeri = (List<Menadzer>)HttpContext.Application["MENADZERI"];
            List<Administrator> admini = (List<Administrator>)HttpContext.Application["ADMINISTRATORI"];

            foreach(Turista tr in turisti)
            {
                if(tr.KorisnickoIme == korisnickoIme)
                {
                    ViewBag.Greska = "Korisnicko ime je vec zauzeto!";
                    RedirectToAction("/Registracija/Registracija");
                }
            }

            foreach (Menadzer m in menadzeri)
            {
                if (m.KorisnickoIme == korisnickoIme)
                {
                    ViewBag.Greska = "Korisnicko ime je vec zauzeto!";
                    RedirectToAction("/Registracija/Registracija");
                }
            }

            foreach (Administrator a in admini)
            {
                if (a.KorisnickoIme == korisnickoIme)
                {
                    ViewBag.Greska = "Korisnicko ime je vec zauzeto!";
                    RedirectToAction("/Registracija/Registracija");
                }
            }


            DateTime datum2 = (DateTime)datum;

            Turista t = new Turista(korisnickoIme, lozinka, ime, prezime, pol, email, datum2, EUloga.turista, new List<Rezervacija>(), false);

            turisti.Add(t);

            HttpContext.Application["TURISTI"] = turisti;

            HttpContext.Session["USER"] = t;

            ViewBag.Aranzmani = (List<Aranzman>)HttpContext.Application["ARANZMANI"];
            ViewBag.Dobrodosli = $"Dobrodosli {korisnickoIme}!";
            return View("~/Views/Turista/Index.cshtml");

        }
    }
}